/* test_matrix.c */
#include "helper.h"

int
main (int argc, char *argv[])
{
  double A[] = { 
    0.11, 0.12, 0.13,
    0.21, 0.22, 0.23
  };
  double B[] = { 
    1011, 1012,
    1021, 1022,
    1031, 1032
  };
  double C[] = { 
    0.00, 0.00,
    0.00, 0.00
  };

  print_matrix (A, 2, 3); /* A is a 2x3 matrix (row-major ordering) */
  print_matrix (B, 3, 2); /* B is a 3x2 matrix (row-major ordering) */
  print_matrix (C, 2, 2); /* C is a 2x2 matrix (row-major ordering) */

  return 0;
}
