/* helper.c */
#include <stdio.h>

void
print_matrix (double *a, int n_row, int n_col)
{
  int k, ell;
  for (k = 0; k < n_row; k++)
    {
      for (ell = 0; ell < n_col; ell++)
	{
	  printf ("%10.5f", *a++);  /* Walk through array with pointer */
	  printf (" ");		    /* 2 spaces between entries */
	}
      printf ("\n");		    /* End of line @ end of row */
    }
}


