#include <stdio.h>
#include <math.h>
void
main (int argc, char *argv[])
{
  printf ("Hello, World!\n");
  double sqrt3, cos_val;
  sqrt3 = sqrt(3.0);
  cos_val = 2.0*cos(M_PI/6.0);
  printf("The value of cos_val is %15.12f\n", cos_val);
  printf("The value of sqrt(3) is %15.12f\n", sqrt3);
  printf("The difference is       %15.12g\n", fabs(cos_val-sqrt3));
}
