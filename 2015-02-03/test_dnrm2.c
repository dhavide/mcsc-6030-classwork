/* test_dnrm2.c */
#include <stdio.h>		/* Needed for printf */
#include <stdlib.h>		/* Needed for strtol, malloc, and free */
#include <math.h>		/* Needed for sqrt */
#include "helper.h"		/* Needed for print_matrix */
#include <cblas.h>		/* Needed for cblas_dnrm2 */

void
main (int argc, char *argv[])
{
  int N;
  double *a;			/* Pointer to array we will allocate */
  char *err;			/* Error flag for strtol */

  N = (int) strtol (argv[1], &err, 10); /* Parse command-line args with error-checking */
  if ((*err) || argc != 2 || N <= 0)
    {
      printf ("%s: Invalid input arguments.\n", argv[0]);
      printf ("Expected: %s N, where N is a positive integer.\n", argv[0]);
      printf ("Received: ");
      for (int k = 0; k < argc; k++)
	printf ("%s  ", argv[k]);
      printf ("\n");
      exit (0);
    }

  a = malloc (N * sizeof (*a)); /* Allocates memory... */

  if (a == NULL)
    {
      printf ("%s: error allocating memory for %d doubles.\n", argv[0], N);
      exit (0);
    }

  for (int k = 0; k < N; k++)
    {
      a[k] = 1.0 + (double) k;
    }

  if (N <= 10)
    print_matrix (a, N, 1);	/* Print as column vector */
  else
    printf ("%10.5f\n%10.5f\n    ...\n%10.5f\n", a[0], a[1], a[N - 1]);
  /* Print abridged version of column vector */

  printf ("%15.12g : Exact norm\n",
	  sqrt (1.0 / 6.0 * N * (N + 1) * (2 * N + 1)));
  double a_nrm2 = cblas_dnrm2 (N, a, 1);
  printf ("%15.12g : Norm as computed by cblas_dnrm2.\n", a_nrm2);

  free (a);			/* Clean up memory */
}
