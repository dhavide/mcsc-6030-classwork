#include <stdio.h>

int main(int argc, char *argv[]) {

printf("Hello, world!\n");
printf("There are %d command-line arguments!\n",argc);

for (int k=0; k<argc; k++) 
   printf("%s\n",argv[k]);

printf("This gets executed once with a %% character.\n");

return 0;
}
