#include <stdio.h>
int main(int argc, char *argv[]) {

int N_pts, k;
double delta_x, x_k, pi_est;

N_pts = atoi(argv[1]);
delta_x = 1.0/N_pts;

pi_est = 0.0;
for (k=0; k<N_pts; k++) {
  x_k = (k+0.5) * delta_x;
  pi_est += 1.0/(1.0+x_k*x_k);
}

pi_est *= (4.0*delta_x);
printf("%17.15f\n", pi_est);

return 0;
}
