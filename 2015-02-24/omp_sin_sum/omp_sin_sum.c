#include "omp_sin_sum.h"
/* File:    omp_sin_sum.c
 * Purpose: Compute a sum in which each term is the value of a function
 *          applied to a nonnegative integer i and evaluation of the 
 *          function requires work proportional to i.
 *
 * Compile: gcc -g -Wall -fopenmp -I. -o omp_sin_sum omp_sin_sum.c
 * Usage:   ./omp_sin_sum <number of threads> <number of terms>
 *
 * Input:   none
 * Output:  sum of n terms and elapsed time to compute the sum
 *
 * Notes:
 * 1.  The computed sum is
 *
 *        sin(0) + sin(1) + . . . + sin(n(n+3)/2)
 *
 * 2.  The function f(i) is
 *
 *        sin(i(i+1)/2) + sin(i(i+1)/2 + 1) + . . . + sin(i(i+1)/2 + i)
 *
 * 3.  The parallel for directive uses a runtime schedule clause. So
 *     the environment variable OMP_SCHEDULE should be either
 *     "static,n/thread_count" for a block schedule or "static,1"
 *     for a cyclic schedule
 * 4.  Uses the OpenMP library function omp_get_wtime to take timings.
 * 5.  DEBUG flag will print which iterations were assigned to each
 *     thread.
 *
 * IPP:  Section 5.7 (pp. 236 and ff.)
 */

#ifdef DEBUG
int*    iterations;
#endif

int main(int argc, char* argv[]) {
   double  global_result;        /* Store result in global_result */
   long    n;                    /* Number of terms               */
   int     thread_count;
   double  start, finish;
   double  error, check;

   if (argc != 3) Usage(argv[0]);
   thread_count = strtol(argv[1], NULL, 10);
   n = strtol(argv[2], NULL, 10);
#  ifdef DEBUG
   iterations = malloc((n+1)*sizeof(int));
#  endif

   start = omp_get_wtime();
   global_result = Sum(n, thread_count); /* OMP directives within Sum */
   finish = omp_get_wtime();

   check = Check_sum(n, thread_count);
   error = fabs(global_result - check);

   printf("Result = %.14e\n", global_result);
   printf("Check = %.14e\n", check);
   printf("With n = %ld terms, the error is %.14e\n", n, error);
   printf("Elapsed time = %e seconds\n", finish-start);
#  ifdef DEBUG
   Print_iters(iterations, n);
   free(iterations);
#  endif

   return 0;
}  /* main */
