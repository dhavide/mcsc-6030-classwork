#include "omp_sin_sum.h"

/*--------------------------------------------------------------------
 * Function:    Usage
 * Purpose:     Print command line for function and terminate
 * In arg:      prog_name
 */
void Usage(char* prog_name) {

   fprintf(stderr, "usage: %s <number of threads> <number of terms>\n", 
         prog_name);
   exit(0);
}  /* Usage */

/*------------------------------------------------------------------ */
double Check_sum(long n, int thread_count) {
   long i;
   long finish = n*(n+3)/2;
   double check = 0.0;

#  pragma omp parallel for num_threads(thread_count) \
      default(none) shared(n, finish) private(i) \
      reduction(+: check)
   for (i = 0; i <= finish; i++) {
      check += sin(i);
   }
   return check;
}  /* Check_sum */

/*------------------------------------------------------------------
 * Function:  Print_iters
 * Purpose:   Print which thread was assigned which iteration.
 * Input args:  
 *    iterations:  iterations[i] = thread assigned iteration i
 *    n:           size of iterations array
 */
void Print_iters(int iterations[], long n) {
   int i, start_iter, stop_iter, which_thread;

   printf("\n");
   printf("Thread\t\tIterations\n");
   printf("------\t\t----------\n");
   which_thread = iterations[0];
   start_iter = stop_iter = 0;
   for (i = 0; i <= n; i++)
      if (iterations[i] == which_thread)
         stop_iter = i;
      else {
         printf("%4d  \t\t%d -- %d\n", which_thread, start_iter,
               stop_iter);
         which_thread = iterations[i];
         start_iter = stop_iter = i;
      }
   printf("%4d  \t\t%d -- %d\n", which_thread, start_iter,
               stop_iter);
}  /* Print_iters */
