#include <stdio.h>
#include <math.h>
#include <omp.h>

int fcncount = 0; /* Global variable (file scope) */
double z = 1.0;   /* Global variable (file scope) */

/* Non-thread-safe function (uses global variables) */
double myfcn (double x) {
   fcncount += 1;                 /* counts function calls    */
   z = sin(x);                    /* modifies global variable */
   return (z*cos(x) + fcncount);  /* depends on z & fcncount! */
}

/* Program to illustrate use of non-thread-safe function */
int
main (int argc, char *argv[])
{
  int k, n = 10;
  double x[10] = { -10.0, 11.0, 100.4, 50.0, 32.0,
                    43.1, 55.0, -1001.0, 56.3, 0.0 };
  double y = 0.0;

  /* Print header for intermediate results */
  printf ("%2s\t%10s\t%5s\n", "k", "x[k]", "thread");
  #pragma omp parallel for reduction(+: y)
  for (k = 0; k < n; k++)
    {
      x[k] = myfcn(x[k]);
      y += x[k];
      printf ("%2d\t%10.2f\t%2d\n", k, x[k], omp_get_thread_num ());
    }
  printf ("\ntotal of x = %g\n", y);
  return 0;
}
