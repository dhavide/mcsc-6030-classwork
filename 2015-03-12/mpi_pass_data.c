#include <mpi.h>
#include <stdio.h>

int main(int argc, char *argv[])  {
  int n_proc, proc_id, dest, source, rc, count, tag=4;
  char inmsg, outmsg;
  MPI_Status my_status;

  /* 1. Initialise MPI environment */
  MPI_Init( &argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &n_proc);
  MPI_Comm_rank(MPI_COMM_WORLD, &proc_id);

  /* 2. Conditional send/receives for processes 0 and 1 */
  if (proc_id == 0) {
    outmsg = 'a';
    dest = 1;
    rc = MPI_Send( &outmsg, 1, MPI_CHAR, dest, tag, MPI_COMM_WORLD);
    source = 1;
    rc = MPI_Recv( &inmsg, 1, MPI_CHAR, source, tag, MPI_COMM_WORLD, &my_status);
  } else if (proc_id == 1) {
    outmsg = 'z';
    source = 0;
    rc = MPI_Recv(&inmsg, 1, MPI_CHAR, source, tag, MPI_COMM_WORLD, &my_status);
    dest = 0;
    rc = MPI_Send(&outmsg, 1, MPI_CHAR, dest, tag, MPI_COMM_WORLD);
  }

  rc = MPI_Get_count( &my_status, MPI_CHAR, &count);
  printf("Task %d: Received %d char (namely %c) from task %d with tag %d \n",
         proc_id, count, inmsg, my_status.MPI_SOURCE, my_status.MPI_TAG);

  /* 3. Cleaning up the MPI environment */
  MPI_Finalize();
}
