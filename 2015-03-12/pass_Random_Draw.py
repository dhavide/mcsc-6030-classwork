# pass_Random_Draw.py
# Simple Python program to illustrate Sends & Receives in MPI
# Note: This program hangs with fewer than 2 processes.
#
# 1. Typical set-up code: importing libraries, starting MPI
import numpy as np
from mpi4py import MPI
comm = MPI.COMM_WORLD
proc_id = comm.Get_rank()

# 2. Initialise data (singleton arrays on all processes)
if proc_id==1:
   data = np.random.rand(2) # Generates random double in (0,1)
else:
   data = np.zeros(2, dtype=np.float64)
    
# 3. Branch based on proc_id
if proc_id == 1:
   print "Process", proc_id, "drew:", data[:]
   # Send *from* buffer data to destination using tag_send
   tag_send = 3
   destination = 0
   comm.Send(data, dest=destination, tag=tag_send)
if proc_id == 0:
   print "Process", proc_id, "before Recv has:" , data[:]
   # Receive *into* buffer data from src using tag_recv
   src = 1
   # Initialise the MPI.Status object; this allows us to query properties
   # of the call to MPI.Recv (e.g., how much data was really received,
   # whether errors occurred, which source the messagae came from, etc.)
   my_status = MPI.Status() # Constructor for MPI.Status object
   comm.Recv(data, source=MPI.ANY_SOURCE, tag=MPI.ANY_TAG, \
             status=my_status)
   print "Process", proc_id, "after Recv has:" , data[:]
   tag_recv = my_status.Get_tag()
   print "Process %d: tag_recv = %d, received..." % \
         (proc_id, tag_recv)
   print "Process %d: Error message: %s" % \
         (proc_id, my_status.Get_error())
   print "Process %d: # bytes received = %d" % \
         (proc_id, my_status.Get_count())
