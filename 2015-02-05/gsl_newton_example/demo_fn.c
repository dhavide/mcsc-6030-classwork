#include "demo_fn.h"

/* Implementations of function objects for root-finder */

/* Simple quadratic polynomial
   with coefficents as parameters */
double
quadratic (double x, void *params)
{
  struct quadratic_params *p 
    = (struct quadratic_params *) params;

  double a = p->a;
  double b = p->b;
  double c = p->c;

  return (a * x + b) * x + c;
}

/* Derivative of quadratic polynomial (linear polynomial) */
double
quadratic_deriv (double x, void *params)
{
  struct quadratic_params *p 
    = (struct quadratic_params *) params;
  /* Extract coefficients from parameter structure p */
  double a = p->a;
  double b = p->b;
 
  return 2.0 * a * x + b;
}

/* quadratic_fdf combines quadratic and quadratic_deriv,
   i.e., function object that returns f(x) and f'(x)
   through input arguments y and dy */
void
quadratic_fdf (double x, void *params, 
               double *y, double *dy)
{
  struct quadratic_params *p 
    = (struct quadratic_params *) params;
  /* Extract coefficients from parameter structure p */
  double a = p->a;
  double b = p->b;
  double c = p->c;
  /* Evaluate function values to return in y and dy */
  *y = (a * x + b) * x + c;
  *dy = 2.0 * a * x + b;
}
