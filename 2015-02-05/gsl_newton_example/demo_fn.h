/* qudaratic_params are coefficients of a*x^2+b*x + c */
struct quadratic_params
  {
    double a, b, c;
  };

double quadratic (double x, void *params);
double quadratic_deriv (double x, void *params);
/* quadratic_fdf combines quadratic and quadratic_deriv,
   i.e., function object that returns f(x) and f'(x)
   through input arguments y and dy */
void quadratic_fdf (double x, void *params, 
                    double *y, double *dy);
