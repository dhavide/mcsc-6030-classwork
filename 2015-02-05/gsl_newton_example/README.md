This directory contains code modified from an example in the GNU Scientific
Library documentation (see [Root Finding Examples](http://www.gnu.org/software/gsl/manual/html_node/Root-Finding-Examples.html#Root-Finding-Examples)).

The example problem to solve is to find a root of a simple quadratic polynomial equation

$$ f(x) = ax^2 + bx + c = 0 $$

where $a$, $b$, and $c$ are prescribed coefficients. The file ``demo_fn.c``
contains implementations of the function $f$ and its derivative $f'$. There
is a surprising amount of code to implement such a simple problem. This is in
part due to the ``params`` structure used to pass the coefficients of the
polynomial and also due to the third function ``fdf`` that computes both
$f$ and its derivative $f'$ at $x$. The strength of this approach is that it
can be generalised to arbitrary root-finding problems (with parameters)
fairly readily.
