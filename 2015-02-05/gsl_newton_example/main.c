#include <stdio.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_roots.h>
#include "demo_fn.h" /* Header describing functions */

int
main (void)
{
  /* Initialise function object for rootfinding solver */
  gsl_function_fdf FDF;
  FDF.f = &quadratic;
  FDF.df = &quadratic_deriv;
  FDF.fdf = &quadratic_fdf;
  struct quadratic_params params = {1.0, 0.0, -5.0};
  FDF.params = &params;

  /* Select algorithm for rootfinding solver */
  const gsl_root_fdfsolver_type *T;
  T = gsl_root_fdfsolver_newton;
  /* Prepare memory for rootfinding solver */
  gsl_root_fdfsolver *s;
  s = gsl_root_fdfsolver_alloc (T);
  /* Associate selected solution algorithm with function
     object FDF associated with problem at hand */
  double x_current;
  x_current = 3.0;
  gsl_root_fdfsolver_set (s, &FDF, x_current);

  /* Prepare for output before main loop */
  printf ("using %s method\n", 
          gsl_root_fdfsolver_name (s));
  printf ("%-5s %-15s %-15s %-15s\n",
          "iter", "root", "err", "err(est)");

  /* Prepare parameters to control iteration */
  int status;
  int iter = 0, max_iter = 100;
  double x_next, r_expected, TOL;
  x_next = x_current;
  r_expected = sqrt (5.0);
  TOL = 1.0e-6;
  do
    {
      iter++;
      /* Compute next Newton iterate */
      status = gsl_root_fdfsolver_iterate (s);
      x_current = x_next;
      /* Extract current iterate from s into x_next */
      x_next = gsl_root_fdfsolver_root (s);
      /* Check convergence criteria */
      status = gsl_root_test_delta (x_next, x_current, 0, TOL);
      if (status == GSL_SUCCESS)
        printf ("Converged:\n");

      printf ("%-5d %-15.7f %-15.7e %-15.7e\n",
              iter, x_next, fabs(x_next - r_expected), fabs(x_next - x_current));
    }
  while (status == GSL_CONTINUE && iter < max_iter);

  /* Clean up memory associated with solver object */
  gsl_root_fdfsolver_free (s);
  return status;
}
