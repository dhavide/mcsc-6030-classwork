/* Makefile-examples/fullcode.c */
#include <stdio.h>

void
fun1 (void)
{
  printf ("In fun1\n");
}

void
fun2 (void)
{
  printf ("In fun2\n");
}

/* Main driver program */
int
main (int argc, char *argv[])
{
  printf ("In main\n");
  fun1 ();
  fun2 ();
  return 0;
}
