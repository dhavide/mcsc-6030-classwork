/* Makefile-examples/main.c */
#include <stdio.h>
#include "multifile.h"

/* Main driver program */
int
main (int argc, char *argv[])
{
  printf ("In main\n");
  fun1 ();
  fun2 ();
  return 0;
}
