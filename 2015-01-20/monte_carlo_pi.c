#include <stdio.h>
#include <stdlib.h>
int main(int argc, char *argv[])
{
int k, N_trials, N_inside, seed;
double x, y, pi_est;
N_trials = atoi(argv[1]); /* Number of draws from command-line */
seed = atoi(argv[2]);     /* Random seed from command line */
srand(seed);  /* Seeds iteration for pseudo-random number generator */
N_inside = 0;
for (k=0; k<N_trials; k++) /* Principal loop */
{
   /* x & y drawn from uniform random distribution on [0,1] */
   x = (double) rand()/RAND_MAX;
   y = (double) rand()/RAND_MAX;
   if ((x*x + y*y)<1.0)
      N_inside++;
}
/* Calculate estimate; use cast to avoid integer arithmetic */
pi_est = 4.0*(double) N_inside/N_trials;     
printf("%17.15f\n", pi_est);
return 0;
}
