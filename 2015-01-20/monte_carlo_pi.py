# monte_carlo_pi.py
import sys
import random

N_trials = int(sys.argv[1]) # Get command-line argument
N_inside = 0 
for k in range(N_trials):
    x = random.random() # uniform between 0 and 1
    y = random.random() # uniform between 0 and 1
    if (x**2 + y**2 < 1):
        N_inside += 1

pi_est = 4.0 * N_inside / float(N_trials)
print "%17.15f" % pi_est
