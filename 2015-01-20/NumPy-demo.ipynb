{
 "metadata": {
  "name": ""
 },
 "nbformat": 3,
 "nbformat_minor": 0,
 "worksheets": [
  {
   "cells": [
    {
     "cell_type": "heading",
     "level": 1,
     "metadata": {},
     "source": [
      "Introduction to NumPy"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Additional resources:\n",
      "* [NumPy Tutorial](http://www.scipy.org/Tentative_NumPy_Tutorial)\n",
      "* [NumPy/SciPy documentation](http://docs.scipy.org/doc/)\n",
      "\n",
      "As usual, we need to begin by importing the ``numby`` library."
     ]
    },
    {
     "cell_type": "heading",
     "level": 2,
     "metadata": {},
     "source": [
      "Basics"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "import numpy as np # Favour this to \"from numpy import *\""
     ],
     "language": "python",
     "metadata": {},
     "outputs": [],
     "prompt_number": 1
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "The import command above is preferable for (at least) two reasons:\n",
      "\n",
      "* the namespace of your Python session will be cleaner (i.e., less opportunity for collision of identifiers)\n",
      "* the `ipython` interpreter can use tab-completion to assist you"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "The primary object in `numpy` is `ndarray`, a homogeneous, multidimensional array (typically of numbers, but conceivably other data structures). The major attributes of `ndarray` are:\n",
      "\n",
      "* **`ndarray.ndim`**:  the *rank* (i.e., number of axes or dimensions) of the array.\n",
      "* **`ndarray.shape`**: the *shape* of the array is a tuple of integers indicating the extent of the array in each dimension. For example, a matrix `A` with `n` rows and `m` columns has `A.shape==(n,m)`. The length of the shape tuple is therefore the rank, or number of dimensions, `ndim`.\n",
      "* **`ndarray.size`**: the total number of elements of the array. This is equal to the product of the elements of shape.\n",
      "* **`ndarray.dtype`**: the datatype of the elements in the array. The `dtype` can be a standard Python type or user-defined. Additionally, NumPy provides types of its own (e.g., `numpy.int32`, `numpy.int16`, `numpy.float64`, etc.).\n",
      "* **`ndarray.itemsize`**: the size in bytes of each element of the array. For example, an array of `B` of elements of type `float64` has `B.itemsize==8` (64 bits/8 bits/byte), while an array `C` of type `complex32` has `C.itemsize==4` (32 bits/8 bits/byte). The value of `ndarray.itemsize` is equivalent to `ndarray.dtype.itemsize`."
     ]
    },
    {
     "cell_type": "heading",
     "level": 2,
     "metadata": {},
     "source": [
      "Creating arrays"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Arrays can be created by wrapping lists or tuples in the `numpy.array` command."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "a = np.array((1, 2, 3.0, 10)) # rank 1 array of shape (4,) of 8 byte integers\n",
      "print a\n",
      "print a.shape\n",
      "print a.dtype.name  # 8-byte integer inferred from input tuple"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "[  1.   2.   3.  10.]\n",
        "(4,)\n",
        "float64\n"
       ]
      }
     ],
     "prompt_number": 5
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "b = np.array( [ [1.3, 4.5, 7.6, -1.2], [-5, 3.2, 4, -1.03 ] ], dtype=np.float32 )\n",
      "# rank 2 array of shape (2,4) of single precision floats\n",
      "print b\n",
      "print b.shape\n",
      "print b.dtype.name # 8-byte floats inferred from input lists of data"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "[[ 1.29999995  4.5         7.5999999  -1.20000005]\n",
        " [-5.          3.20000005  4.         -1.02999997]]\n",
        "(2, 4)\n",
        "float32\n"
       ]
      }
     ],
     "prompt_number": 6
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Usually, arrays are not created using the `numpy.array` command. There are a variety of more convenient utilities for array creation.\n",
      "\n",
      "<center>\n",
      "<table>\n",
      "\t<tr><td><tt>numpy.zeros</tt></td><td>Array of zeros</td></tr>\n",
      "\t<tr><td><tt>numpy.zeros_like</tt></td><td>Array of zeros matching shape and dtype of input array</td></tr>\n",
      "\t<tr><td><tt>numpy.ones</tt></td><td>Array of ones</td></tr>\n",
      "\t<tr><td><tt>numpy.ones_like</tt></td><td>Array of ones matching shape and dtype of input array</td></tr>\n",
      "    <tr><td><tt>numpy.arange</tt></td><td>Integers in a specified range</td></tr>\n",
      "\t<tr><td><tt>numpy.linspace</tt></td><td>Linearly spaced samples</td></tr>\n",
      "    <tr><td><tt>numpy.logspace</tt></td><td>Logarithmically spaced samples</td></tr>\n",
      "    <tr><td><tt>numpy.random.randn</tt></td><td>Random samples from standard Gaussian distribution</td></tr>\n",
      "    <tr><td><tt>numpy.random.rand</tt></td><td>Random samples from uniform distribution</td></tr>\n",
      "</table>\n",
      "</center>"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": true,
     "input": [
      "c = np.linspace(1,5,11) # creates array of 11 elements linearly spaced between 1 and 11 (including 11)\n",
      "print c\n",
      "z = np.zeros_like(c)  # this will give an array of aeros the same shape *and* datatype as array a defined above\n",
      "print z, z.dtype, z.shape"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "[ 1.   1.4  1.8  2.2  2.6  3.   3.4  3.8  4.2  4.6  5. ]\n",
        "[ 0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.] float64 (11,)\n"
       ]
      }
     ],
     "prompt_number": 8
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "d = np.arange(3,7)      # Creates integer array of 4 elements starting at 3 stopping strictly below 7\n",
      "print d"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "[3 4 5 6]\n"
       ]
      }
     ],
     "prompt_number": 9
    },
    {
     "cell_type": "heading",
     "level": 3,
     "metadata": {},
     "source": [
      "Reshaping and transposing arrays"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "When reshaping arrays of dimension two or higher, the underlying ordering scheme determines how the data fills in the array.\n",
      "\n",
      "* With row-major (C-style) ordering, the rows are filled up first, columns afterward.\n",
      "* With column-major (FORTRAN-style) ordering, columns are filled in first.\n",
      "\n",
      "The default in `numpy` is C-style. Row- and column-major ordering can be generalised to higher-dimensional arrays."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "a = np.arange(20)\n",
      "a_F = a.reshape(4,5)\n",
      "a_C = a.reshape(4,5,order='C') # default ordering is C (row-major)\n",
      "print a\n",
      "print a_F\n",
      "print a_C"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "[ 0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19]\n",
        "[[ 0  1  2  3  4]\n",
        " [ 5  6  7  8  9]\n",
        " [10 11 12 13 14]\n",
        " [15 16 17 18 19]]\n",
        "[[ 0  1  2  3  4]\n",
        " [ 5  6  7  8  9]\n",
        " [10 11 12 13 14]\n",
        " [15 16 17 18 19]]\n"
       ]
      }
     ],
     "prompt_number": 12
    },
    {
     "cell_type": "heading",
     "level": 2,
     "metadata": {},
     "source": [
      "Indexing, slicing, and manipulating arrays"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "One-dimensional arrays are simple. For the most part, they are treated like Python lists. As is typical in Python, arrays are indexed from zero."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "a = np.arange(12)\n",
      "print a\n",
      "print a[5]     # indexing a single element of a\n",
      "print a[4:7]   # indexing a slice or view of a\n",
      "a[4:7] = 12    # overwriting slice of a\n",
      "print a"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "[ 0  1  2  3  4  5  6  7  8  9 10 11]\n",
        "5\n",
        "[4 5 6]\n",
        "[ 0  1  2  3 12 12 12  7  8  9 10 11]\n"
       ]
      }
     ],
     "prompt_number": 13
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "When a scalar value is assigned to a slice of an array, the scalar is *broadcasted* to the entire selection.\n",
      "\n",
      "Be aware that *array slices are views* of an array, i.e., data is not copied so modifications to a view will change the original array. This is particularly confusing to ``numpy`` beginners familiar with other array-oriented programming languages. Similarly, assignment of a new identifier to a previously defined array results in a *shallow copy*."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "a = np.arange(10)\n",
      "b = a[5:8]         # changes entries of a\n",
      "print \"Before: a=%s\" % a\n",
      "print \"Before: b=%s\" % b"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "Before: a=[0 1 2 3 4 5 6 7 8 9]\n",
        "Before: b=[5 6 7]\n"
       ]
      }
     ],
     "prompt_number": 14
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "b[1] = -999  # also changes entries of a\n",
      "print \"After: a=%s\" % a\n",
      "print \"After: b=%s\" % b"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "After: a=[   0    1    2    3    4    5 -999    7    8    9]\n",
        "After: b=[   5 -999    7]\n"
       ]
      }
     ],
     "prompt_number": 15
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "b[:] = -32   # also changes entries of a\n",
      "print \"After: a=%s\" % a\n",
      "print \"After: b=%s\" % b"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "After: a=[  0   1   2   3   4 -32 -32 -32   8   9]\n",
        "After: b=[-32 -32 -32]\n"
       ]
      }
     ],
     "prompt_number": 16
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "c = a    # c is a shallow copy of the previously defined array a\n",
      "print \"Before: a=%s\" % a\n",
      "print \"Before: c=%s\" % c\n",
      "c[-1] = -100\n",
      "print \"After: a=%s\" % a\n",
      "print \"After: c=%s\" % c"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "Before: a=[  0   1   2   3   4 -32 -32 -32   8   9]\n",
        "Before: c=[  0   1   2   3   4 -32 -32 -32   8   9]\n",
        "After: a=[   0    1    2    3    4  -32  -32  -32    8 -100]\n",
        "After: c=[   0    1    2    3    4  -32  -32  -32    8 -100]\n"
       ]
      }
     ],
     "prompt_number": 17
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "A genuine copy of a slice can be obtained using the `copy` method associated with the `ndarray` object."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "a = np.arange(10)\n",
      "b = a[5:8].copy()\n",
      "print \"Before: a=%s\" % a\n",
      "print \"Before: b=%s\" % b\n",
      "b[:] = 3\n",
      "print \"After: a=%s\" % a\n",
      "print \"After: b=%s\" % b # This time, entries of a are unchanged"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "Before: a=[0 1 2 3 4 5 6 7 8 9]\n",
        "Before: b=[5 6 7]\n",
        "After: a=[0 1 2 3 4 5 6 7 8 9]\n",
        "After: b=[3 3 3]\n"
       ]
      }
     ],
     "prompt_number": 18
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Two-dimensional arrays can be indexed recursively (as with lists) or using a comma-separated list of indices directly. Slicing higher-dimensional arrays works similarly to slicing lists or one-dimensional arrays."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "A = np.array([[1,2,3],[4,5,6],[7,8,9]])\n",
      "print A\n",
      "print A[0][2]\n",
      "print A[0,2]   # same as above\n",
      "print A[:,0]   # prints 0th column\n",
      "print A[1:,1:] # prints 2x2 submatrix from rows 1 & 2 and columns 1 & 2"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "[[1 2 3]\n",
        " [4 5 6]\n",
        " [7 8 9]]\n",
        "3\n",
        "3\n",
        "[1 4 7]\n",
        "[[5 6]\n",
        " [8 9]]\n"
       ]
      }
     ],
     "prompt_number": 19
    },
    {
     "cell_type": "heading",
     "level": 3,
     "metadata": {},
     "source": [
      "Boolean indexing"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "It is often convenient to use boolean indexing to create a mask with which to process arrays. This turns out to be much faster than scanning through elements explicitly in `for` loops and using `if` blocks for branching. One thing to note: unlike with array slicing, boolean indexing generates deep copies of array data, i.e., "
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "data = np.random.randn(4,6) # Random matrix, entries normally distributed about 0, standard deviation 1\n",
      "mask = data < 0  # Creates boolean array\n",
      "print mask\n",
      "c = data[mask] # c is not a view/shallow copy, i.e., a new array is constructed\n",
      "print data\n",
      "print c, c.shape\n",
      "c[0] = 4.0\n",
      "data[mask] = np.nan  # replace negative entries with Not a Number\n",
      "print data\n",
      "print c \n"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "[[ True False False False False False]\n",
        " [ True False False  True False  True]\n",
        " [ True  True False  True False  True]\n",
        " [False  True False False False False]]\n",
        "[[-1.47560764  0.02596745  0.57803279  1.1373994   0.76062767  1.11402963]\n",
        " [-0.76051707  0.80660736  0.77452622 -1.06588769  0.60083843 -1.20625835]\n",
        " [-1.26631527 -1.13699782  0.63107    -1.22795786  1.69129106 -0.76514005]\n",
        " [ 1.27764323 -0.98321231  0.49788702  1.44743974  1.58580767  0.66964931]]\n",
        "[-1.47560764 -0.76051707 -1.06588769 -1.20625835 -1.26631527 -1.13699782\n",
        " -1.22795786 -0.76514005 -0.98321231] (9,)\n",
        "[[        nan  0.02596745  0.57803279  1.1373994   0.76062767  1.11402963]\n",
        " [        nan  0.80660736  0.77452622         nan  0.60083843         nan]\n",
        " [        nan         nan  0.63107            nan  1.69129106         nan]\n",
        " [ 1.27764323         nan  0.49788702  1.44743974  1.58580767  0.66964931]]\n",
        "[ 4.         -0.76051707 -1.06588769 -1.20625835 -1.26631527 -1.13699782\n",
        " -1.22795786 -0.76514005 -0.98321231]\n"
       ]
      }
     ],
     "prompt_number": 20
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Let's verify that the preceding strategy is indeed faster than using `for` loops and `if` blocks. This example illustrates the use of the `ipython` \"magic\" `%timeit` to make constructing timed experiments easy. "
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "def mask_test(x):\n",
      "    y = x.copy()\n",
      "    y[x<0] = np.nan\n",
      "    return x\n",
      "def no_mask_test(x):\n",
      "    y = np.empty(x.shape)\n",
      "    for k in range(x.shape[0]):\n",
      "        for ell in range(x.shape[1]):\n",
      "            if x[k,ell] < 0:\n",
      "                y[k,ell] = np.nan\n",
      "            else:\n",
      "                y[k,ell] = x[k,ell]\n",
      "    return y"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "n = 5  # Increase value of n to get different timings...\n",
      "a = np.random.randn(n,n)\n",
      "print \"First using mask_test...\"\n",
      "%timeit mask_test(a)\n",
      "print \"Second using no_mask_test...\"\n",
      "%timeit no_mask_test(a)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "heading",
     "level": 2,
     "metadata": {},
     "source": [
      "Universal functions"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Universal functions (or `ufunc`s) are function that operate on `ndarrays` element-by-element. Ufuncs support array broadcasting, type casting, and several other\n",
      "standard features. That is, a ufunc is a \u201cvectorized\u201d wrapper for a function that takes a fixed number of scalar inputs and produces a fixed number of scalar outputs. Examples include `add`, `subtract`, `multiply`, `exp`, `log`, `sin`, `cos`, etc. Again, this is typical of array-oriented, data parallel languages like **MATLAB**."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "a = np.round(10*np.random.randn(3,4)) # normally distributed random numbers with mean 0 and standard deviation 1\n",
      "print a * a              # elementwise product\n",
      "print a ** 2             # elementwise power, same as above\n",
      "print np.sin(a)          # elementwise sine function\n",
      "a += np.exp(a)           # elementwise exponentiation and in-place increment\n",
      "print a"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Universal functions run much faster than ``for`` loops (which should be avoided whenever possible). We can illustrate this in a timed experiment."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "n = 10\n",
      "a = np.random.randn(n,n)  # Generate 500x500 random matrices\n",
      "b = np.random.randn(n,n)\n",
      "def test_ufunc(x,y):  # function that uses ndarray elementwise operations\n",
      "    return np.cos(x*y)\n",
      "def test_for(x,y):  # function that uses explicit element indexing and for loops\n",
      "    c = np.empty(x.shape)\n",
      "    for k in range(x.shape[0]):\n",
      "        for ell in range(x.shape[1]):\n",
      "            c[k,ell] = np.cos(x[k,ell]*y[k,ell])\n",
      "    return c\n",
      "print \"Testing test_ufunc using universal function:\"\n",
      "%timeit test_ufunc(a,b) # use ipython magic %timeit to measure performance \n",
      "print \"Testing test_for using for loops:\"\n",
      "%timeit test_for(a,b) # use ipython magic %timeit to measure performance"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Here is a one-line vectorised implementation of the sinc function, i.e.,\n",
      "$ \\displaystyle{\\text{sinc}(x) = \\frac{\\sin (\\pi x)}{\\pi x}}$ whenever $x\\neq 0$ and $\\text{sinc}(0) = 1.$"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "def sinc(z):\n",
      "    TOL = 1.0e-12\n",
      "    y = np.where(abs(z)<TOL, 1.0, np.sin(np.pi*z)/(np.pi*z))  # One-line implementation of sinc(z)\n",
      "    return y"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "%matplotlib inline\n",
      "import matplotlib.pylab as mpl\n",
      "x = np.linspace(-6,6,101)\n",
      "y = sinc(x)\n",
      "mpl.plot(x,y,'ro')\n",
      "mpl.show()"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "heading",
     "level": 2,
     "metadata": {},
     "source": [
      "Basic statistics and related operations"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Common elementary statistical operations are implemented as array methods in `numpy`. In partucular, `sum`, `mean`, and `std` are easily invoked as array methods. There are also a top-level `numpy` functions to do these operations."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "A = np.round(10*np.random.randn(3,6))\n",
      "print A\n",
      "print A.mean()  # mean of all the elements of A\n",
      "print A.std()   # standard deviation of all the elements of A\n",
      "print A.sum()   # sum of all the elements of A\n",
      "print A.sum(axis=0) # summing along dimension 0\n",
      "print A.sum(axis=1) # summing along dimension 1"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "For reduction operations like `sum` or `prod`, it is also possible to compute cumulative sums or products."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "print A.cumsum(axis=0)\n",
      "print A.cumsum(axis=1)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "One thing to be aware of when dealing with floating point numbers is that rounding errors can alter computed results in subtle ways. This is not always noticeable, but such effects make testing of numerical software difficult.\n",
      "\n",
      "For instance, arguments from complex variables show that $\\sum_{n=1}^{\\infty} \\frac{1}{n^2} = \\frac{\\pi^2}{6}$. We can compute the first $N$ terms of this sequence in a `numpy` array of single-precision `float32` elements. We can then use the `sum()` method to compute the sum. We can also use slicing to reverse the order of the terms in the array and sum them in reverse order (in which the individual terms are monotonically increasing in magnitude)."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "N = 6 # Use a small value first\n",
      "inv_squares = np.array( [ n for n in range(1,N+1) ], dtype=np.float32 )  # Use a list comprehension to generate the terms\n",
      "print inv_squares         # Displays whole array; comment out for N large\n",
      "print inv_squares[::-1]   # Displays whole array in reverse order; comment out for N large\n",
      "#print 'The sum added forward is %f15.' % inv_squares.sum()\n",
      "#print 'The sum added backward is %f15.' % inv_squares[::-1].sum()"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "In this particular case, the result of summing a sequence of numbers is different when the terms are summed in a different order. Subtleties like this are not disastrous, but they do make testing of numerical software difficult, especially in parallel computing (for instance, dividing a sum up among numerous processors that finish their tasks in a nondeterministic order leads to different results)."
     ]
    },
    {
     "cell_type": "heading",
     "level": 2,
     "metadata": {},
     "source": [
      "Linear algebra"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "The `scipy` website contains [documentation for `numpy.linalg` linear algebra routines](http://docs.scipy.org/doc/numpy/reference/routines.linalg.html).\n",
      "Most of the dense linear algebra routines in `numpy` are accessible through the submodule `numpy.linalg`. Notable exceptions are:\n",
      "\n",
      "<center>\n",
      "<table>\n",
      "<tr><td><tt>numpy.dot(A,B)</tt></td><td>matrix-matrix product</td></tr>\n",
      "<tr><td><tt>numpy.vdot(x,y)</tt></td><td>inner product of vectors</td></tr>\n",
      "<tr><td><tt>numpy.inner(x,y)</tt></td><td>inner product of vectors (without complex conjugation)</td></tr>\n",
      "<tr><td><tt>numpy.tensordot(A,B)</tt></td><td>tensor dot product along specified axes</td></tr>\n",
      "<tr><td><tt>numpy.kron(A,B)</tt></td><td>Kronecker product of tensors</td></tr>\n",
      "</table>\n",
      "</center>\n",
      "\n",
      "\n",
      "`numpy.dot` has the same effect as the `ndarray.dot` method."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "A = np.arange(1,26).reshape(5,5)\n",
      "x = np.ones((5,),dtype=np.int64)\n",
      "print A; print A.dtype.name\n",
      "print x;          # Notice x is not printed as a column vector\n",
      "print x.dtype.name\n",
      "print A.dot(x)    # computes matrix-vector product A x\n",
      "print np.dot(A,x) # same as preceding\n",
      "print x.dot(A)    # computes vector-matrix product x^T A\n",
      "print np.dot(x,A) # same as preceding"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "`numpy` does not distinguish one-dimensional arrays as column or row vectors. Thus, when using one-dimensional arrays to compute products in the matrix algebra sense, some care must be taken to ensure that products are computed in the manner intended. If you need to compute an outer product (which is almost never a good idea), the function `np.outer` can do the job."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "x = np.array([1,2,3])\n",
      "y = np.array([4,5,6])\n",
      "print np.dot(x,y)\n",
      "print np.inner(x,y)\n",
      "print np.outer(x,y)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "It can be argued that the **MATLAB** use of the multiplication operator to denote matrix products is simpler than using `np.dot`. However, the `numpy` representation makes it harder to make certain naive mistakes. In particular, novice **MATLAB** users often confuse matrix-matrix products with elementwise products; it can be wiser to force programmers to be deliberate in their choice.\n",
      "\n",
      "As an example, consider computing the product of a rank-one matrix with a vector. This can be written as $\\mathbf{u}\\mathbf{v}^T\\mathbf{x}$, where $\\mathbf{u}$, $\\mathbf{v}$, and $\\mathbf{x}$ are all assumed to be $N\\times1$ column vectors. A novice **MATLAB** programmer might write this as `u*v'*x`. As matrix products are associative, in theory, it does not matter whether or not this product is computed as $(\\mathbf{u}\\mathbf{v}^T)\\mathbf{x}$ or as $\\mathbf{u}(\\mathbf{v}^T\\mathbf{x})$. Let's try this out using `numpy.dot` (we could use `numpy.inner` and `numpy.outer` to compute the products explicitly)."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "N=4\n",
      "u = np.arange(1,N+1).reshape(N,1)\n",
      "v = np.arange(N+1,2*N+1).reshape(N,1)\n",
      "x = np.arange(2*N+1,3*N+1).reshape(N,1)\n",
      "#print u\n",
      "#print v\n",
      "#print x\n",
      "print \"First using the naive way...\"\n",
      "%timeit np.dot(np.dot(u, v.T),x)  # Naive way\n",
      "print \"Now using the sensible way...\"\n",
      "%timeit np.dot(u, np.dot(v.T,x))  # Less naive way\n",
      "#print np.dot(u, np.dot(v.T,x))"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "In practice, the former product is much worse to compute when $N$ is large; it requires $\\mathcal{O}(N^2)$ storage and operations as compared to $\\mathcal{O}(N)$ storage and operations for the latter product. It can also be shown that the former naive method is also likely to be less accurate because outer products are not backward stable. In any case, using `numpy`, it is more difficult to make this kind of programming error because the programmer has to think more carefully about the products involved."
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Dense linear algebra routines in `numpy.linalg` are wrappers to routines in the BLAS and LAPACK libraries. You almost always want to use these routines in scientific work rather than having to reimplement algorithms developed over decades.\n",
      "\n",
      "<center>\n",
      "<table>\n",
      "    <tr><td><tt>numpy.linalg.solve(A,b)</tt></td><td>solve square matrix equation <tt>np.dot(A,x) = b</tt> for <tt>x</tt></td></tr>\n",
      "    <tr><td><tt>numpy.linalg.lstsq(A,b)</tt></td><td>\"solve\" rectangular matrix equation <tt>np.dot(A,x) = b</tt> in least-squares sense</td></tr>\n",
      "    <tr><td><tt>numpy.linalg.norm(x)</tt></td><td>matrix or vector norm of <tt>x</tt></td></tr>    \n",
      "    <tr><td><tt>numpy.linalg.eig(A)</tt></td><td>compute eigenvalues and right eigenvectors of square matrix <tt>A</tt></td></tr>\n",
      "    <tr><td><tt>numpy.linalg.eigvals(A)</tt></td><td>compute eigenvalues of square matrix <tt>A</tt></td></tr>    \n",
      "    <tr><td><tt>numpy.linalg.qr(A)</tt></td><td>compute QR factorisation of matrix <tt>A</tt></td></tr>\n",
      "    <tr><td><tt>numpy.linalg.svd(A)</tt></td><td>compute singular value decomposition of matrix <tt>A</tt></td></tr>\n",
      "</table>\n",
      "</center>"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "N=10\n",
      "A = np.random.randn(N,N) + np.diag(10*np.ones(N))\n",
      "xtrue = np.random.randn(N,1)\n",
      "b = A.dot(xtrue)  # compute RHS of matrix equation A*xtrue = b\n",
      "xsolve = np.linalg.solve(A,b)\n",
      "print 'Difference between true and computed solution is %15g.' % np.linalg.norm(xtrue-xsolve)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "e = np.linalg.eigvals(A) # Example of a (dense) eigenproblem\n",
      "print e[:5] # Print the first 5 eigenvalues"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    }
   ],
   "metadata": {}
  }
 ]
}