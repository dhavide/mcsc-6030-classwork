# montecarlo.py (based on example from Using MPI (Gropp, Lusk, Skellum)
#
# This program generates a Monte Carlo approximation to Pi by randomly
# generating tuples (x,y) inside the square [-1,1]x[-1,1]. Computing the
# fraction of pairs that lie within the unit disk and multiplying by 4
# (the area of the square) gives an estimate of the area of the unit
# disk (which is Pi). This approach can be generalised for computing
# definite integrals of arbitrary functions in higher dimensions.
#
# The approach here uses one process as a server (the one of highest
# rank) that makes arrays of random numbers to send to the other worker
# processes. The worker processes count of the number of points
# generated inside and outside the unit disk in each set of sample
# points and they collectively accumulate the totals until the estimate
# converges within an acceptable tolerance of the correct value.
#
# Take note of the MPI calls to generate the new communicator for the
# worker processes. This is required so that a reduction can be executed
# that excludes the server process.
from mpi4py import MPI
import numpy as np
import sys
world = MPI.COMM_WORLD
n_proc = world.Get_size()
proc_id = world.Get_rank()

# Define constants to use as messages for the program
REQUEST = 1
REPLY = 2
MAX_ITNS = 10000
PI_TRUE = 3.141592653589793238462643

# Scan size of chunks and tolerance from stdin
CHUNKSIZE = np.array([0])
TOL = np.array([0.])
if proc_id==0:
    CHUNKSIZE[0] = int(sys.argv[1])
    TOL[0] = float(sys.argv[2])
world.Bcast(TOL,0)
world.Bcast(CHUNKSIZE,0)
TOL = TOL[0]              # Redefine TOL as float (not an array)
CHUNKSIZE = CHUNKSIZE[0]  # Redefine CHUNKSIZE as integer (not an array)

# Create new communicator for worker processes
server = n_proc-1                                # ID of server process
worker_group = world.Get_group().Excl([server])  # Extract group
workers = world.Create(worker_group)             # Create communicator
worker_group.Free()                              # Release memory

# Allocate numpy arrays for sending/receiving random numbers, requests
rands = np.empty(2*CHUNKSIZE,dtype=np.float)
request = np.empty(1,dtype=np.int)

# Beginning of actual computation
if proc_id==server:
    request[0] = 1 # Force while loop to go through at least one iteration
    while request[0]==1:
        stat = MPI.Status()
        world.Recv(request, MPI.ANY_SOURCE, tag=REQUEST, status=stat)
        # Generate random samples and send to requesting process
        if request[0]==1:
             rands[:] = np.random.rand(2*CHUNKSIZE)
             world.Send(rands, stat.source, tag=REPLY)
else: # Code to be executed by worker processes
   done = False
   itn = 0
   inside = np.zeros(1,dtype=np.int)
   outside = np.zeros(1,dtype=np.int)
   total_in = np.zeros(1,dtype=np.int)
   total_out = np.zeros(1,dtype=np.int)
   request[0] = 1
   # Initial request for random samples
   world.Send( request, server, tag=REQUEST )
   while not(done):
       itn +=1
       stat = MPI.Status()
       world.Recv(rands,server,tag=REPLY,status=stat)
       # Having received samples from server, begin calculations
       x = 2.0*rands[::2]-1.0
       y = 2.0*rands[1::2]-1.0
       incount = np.count_nonzero(x**2+y**2<1.0)
       inside[0] += incount
       workers.Allreduce(inside, total_in, op=MPI.SUM)
       Pi_est  = (4.0*total_in[0])/(itn*CHUNKSIZE*(n_proc-1))
       # Note: we generally wouldn't know PI_TRUE, so this termination
       # condition would be different.
       err = abs(Pi_est-PI_TRUE)
       done = (err < TOL) | (itn > MAX_ITNS)
       if done:
            if proc_id==0:       # ONLY process 0 sends request to stop
                 request[0] = 0  # Send 0 to instruct server to stop
                 world.Send( request, server, tag=REQUEST )
       else:                     # ALL processes send requests for new samples
            request[0] = 1
            world.Send( request, server, tag=REQUEST )

   # Summary information
   if (proc_id == 0):
       print "\nTotal processes: %d\nTotal parallel iterations: %d\nTotal samples: %d" % \
	 (n_proc,itn, itn*CHUNKSIZE*(n_proc-1))
       print "Computed error is %10.5g" % (err,)
   workers.Free() # After completion, free communicator for workers
