# Illustration of gathering a vector in MPI
# python scatter_example.py n_block
import sys
import numpy as np

from mpi4py import MPI
comm = MPI.COMM_WORLD
n_proc = comm.Get_size()
proc_id = comm.Get_rank()

n_block = int(sys.argv[1])
N = n_block*n_proc

# Only process 0 allocates space for sendbuff
sendbuff = np.linspace(0,1.0,N) if proc_id==0 else None
recvbuff = np.empty(n_block)

print "Proc", proc_id, "has", recvbuff, "before scatter"
if proc_id==0:
    print "Proc", proc_id, "will scatter", sendbuff

comm.Barrier() # synchronisation
# Scatter vector in sendbuff to all processes
comm.Scatter(sendbuff,recvbuff,0)

print "Proc", proc_id, "has", recvbuff

