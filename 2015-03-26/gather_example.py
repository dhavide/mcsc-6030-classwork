# Illustration of gathering a vector in MPI
# python gather_example.py n_block
import sys
import numpy as np

from mpi4py import MPI
comm = MPI.COMM_WORLD
n_proc = comm.Get_size()
proc_id = comm.Get_rank()

n_block = int(sys.argv[1])
N = n_block*n_proc

# Only process 0 allocates space for recvbuff
recvbuff = np.empty(N) if proc_id==0 else None
sendbuff = np.random.rand(n_block) + proc_id

print "Proc", proc_id, "has", sendbuff, "before gather"

comm.Barrier()  # synchronisation
# Gather blocks from all processes
comm.Gather(sendbuff,recvbuff,0)

if proc_id==0:
    print "Proc", proc_id, "has gathered", recvbuff
