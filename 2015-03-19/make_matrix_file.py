# make_matrix_file.py
# Syntax:
#    python make_matrix_file.py M N file_name
# where M and N are positive integers (matrix dimensions) and
# file_name is the name of the output binary file.
#
# Generates binary file containing matrix of 64-bit floats (doubles)
# First two entries are matrix dimensions (32-bit integers)
# Remaining entries are matrix entries (64-bit floats) stored in
# row-major ordering.
# Note: Must modify to use other data types  (e.g., float, double, etc.)
import sys
import numpy as np

# Parse command-line arguments
M = int(sys.argv[1]) # Number of rows
N = int(sys.argv[2]) # Number of columns
file_name = sys.argv[3] # Name of output file

# Generate data to write to disk
A = np.random.rand(M,N) 
# numpy array B stores matrix dimensions since those need to be written to
# disk and we wish to use the numpy tofile method to do the writing
B = np.array( (M,N), dtype=np.int32 )
print A # Comment this line out when M and N are large
print B # Just to verify output is correct

# Write output to disk as binary file
f = open(file_name,'wb')
B.tofile(f)
A.tofile(f)
f.close()

