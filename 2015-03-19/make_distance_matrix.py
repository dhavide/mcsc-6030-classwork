# make_distance_matrix.py
# Syntax:
#    python make_distance_matrix.py M file_name
# where M is a positive integer (matrix dimension M x M) and
# file_name is the name of the output binary file.
#
# Generates binary file containing matrix of 32-bit integers
# First two entries are matrix dimensions (32-bit integers)
# Remaining entries are matrix entries (32-bit integers) stored in
# row-major ordering.
# Note: Must modify to use other data types  (e.g., float, double, etc.)
import sys
import numpy as np

# Parse command-line arguments
M = int(sys.argv[1]) # Number of rows/columns
file_name = sys.argv[2] # Name of output file

# Generate data (integers between 1 & 100)
A = np.int32(np.random.random_integers(1,100,(M,M)))
A[range(M),range(M)] = 0 # Assign 0 to diagonals
# numpy array B stores matrix dimensions since those need to be written to
# disk and we wish to use the numpy tofile method to do the writing
B = np.array( (M,M), dtype=np.int32 )
print A # Comment this line out when M and N are large
print B # Just to verify output is correct

# Write output to disk as binary file
f = open(file_name,'wb')
B.tofile(f)
A.tofile(f)
f.close()

