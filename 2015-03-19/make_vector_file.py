# make_vector_file.py
# Syntax:
#    python make_vector_file.py N file_name
# where N is a positive integer (length of desired vector) and
# file_name is the name of the output binary file.
#
# Generates binary file containing vector of 64-bit floats (doubles)
# First entry is N, vector dimension (32-bit integer)
# Remaining entries are vector entries (64-bit floats) stored in
# row-major ordering.
# Note: Must modify to use other data types  (e.g., float, double, etc.)
import sys
import numpy as np

# Parse command-line arguments (no argument-checking yet)
N = int(sys.argv[1])    # Number of rows
file_name = sys.argv[2] # Name of output file

# Generate data to write to disk
A = np.random.rand( N )
# numpy array B stores vector length since that needs to be written to
# disk and we wish to use the numpy tofile method to do the writing
B = np.array( [N], dtype=np.int32 )
print A # Comment this line out when N is large
print B # Just to verify output is correct

# Write output to disk as *binary* file using numpy methods
f = open(file_name,'wb')
B.tofile(f)
A.tofile(f)
f.close()
