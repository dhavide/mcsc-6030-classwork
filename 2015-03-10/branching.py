# branching.py
# Simple program to demonstrate how branching works in SPMD model

# 1. Typical start up section
from mpi4py import MPI
comm = MPI.COMM_WORLD
n_proc  = comm.Get_size()   # number of processes
rank = comm.Get_rank()   # rank of process

# 2. Assign data; same on all processes
a = 6.0 # same value for *all* processes
b = 3.1 # same value for *all* processes

# 3. Use conditional logic to execute distinct computations on each process
if (rank % 3) == 0:
   print "Proc %d: adding %g+%g=%g" % (rank,a,b,a+b)
elif (rank % 3) == 1:
   print "Proc %d: multiplying %g*%g=%g" % (rank,a,b,a*b)
else:
   print "Proc %d: dividing %g/%g=%g" % (rank,a,b,a/b)

# 4. Clean up (recall MPI.Finalize() not required in mpi4py
