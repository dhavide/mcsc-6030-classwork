# bcastexample.py
# Simple code to illustrate how MPI.bcast works in mpi4py

# 1. Generic start-up portion
import sys
import numpy as np
from mpi4py import MPI
comm = MPI.COMM_WORLD
n_proc = comm.Get_size()
proc_id = comm.Get_rank()

# 2. parsing command-line input; distinct initialisation on each process
N = int(sys.argv[1])
if proc_id==0:
	A = np.random.rand(N,N)
else:
	A = np.empty((N,N),dtype=np.float64)

# 3. Synchronise and print out data on each process *before* broadcast
comm.Barrier() # sychronise
print "Proc ", proc_id, ": " , A

# 4. Carry out actual broadcast (note two distinct syntaxes seem to work)
comm.Bcast(A,0) # broadcast from process 0
#comm.Bcast([A,MPI.DOUBLE],0)

# 5. Synchronise and print out data on each process *after* broadcast
comm.Barrier() # sychronise

if proc_id==0:
   print "========" # To emphasise before vs. after in print out
# Now print value of A on each process after broadcasting
print "Proc ", proc_id, ": " , A

