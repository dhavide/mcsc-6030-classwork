/*
 *   Sieve of Eratosthenes
 *
 *   Programmed by Michael J. Quinn
 *
 *   Last modification: 7 September 2001
 */

#include "mpi.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#define MIN(a,b)  ((a)<(b)?(a):(b))

int main (int argc, char *argv[])
{
   int    count;        /* Local prime count */
   double elapsed_time; /* Parallel execution time */
   int    first;        /* Index of first multiple */
   int    global_count; /* Global prime count */
   int    high_value;   /* Highest value on this proc */
   int    k;
   int    proc_id;      /* Process ID number */
   int    index;        /* Index of current prime */
   int    low_value;    /* Lowest value on this proc */
   char  *marked;       /* Portion of 2,...,'N' */
   int    N;            /* Sieving from 2, ..., 'N' */
   int    n_proc;       /* Number of processes */
   int    prime;        /* Current prime */
   int    n_local;       /* Elements in 'marked' */

   MPI_Init (&argc, &argv);

   /* Start the timer */

   MPI_Comm_rank (MPI_COMM_WORLD, &proc_id);
   MPI_Comm_size (MPI_COMM_WORLD, &n_proc);
   MPI_Barrier(MPI_COMM_WORLD);
   elapsed_time = -MPI_Wtime();

   if (argc != 2) {
      if (!proc_id) printf ("Command line: %s <m>\n", argv[0]);
      MPI_Finalize();
      exit (1);
   }

   N = atoi(argv[1]);

   /* Figure out this process's share of the array, as
      well as the integers represented by the first and
      last array elements */

   low_value = 2 + proc_id*(N-1)/n_proc;
   high_value = 1 + (proc_id+1)*(N-1)/n_proc;
   n_local = high_value - low_value + 1;

   /* Bail out if all the primes used for sieving are
      not all held by process 0 */

   if (n_proc > (int) sqrt((double) N)) {
      if (!proc_id) printf ("Too many processes\n");
      MPI_Finalize();
      exit (1);
   }

   /* Allocate this process's share of the array. */

   marked = (char *) malloc (n_local);

   if (marked == NULL) {
      printf ("Cannot allocate enough memory\n");
      MPI_Finalize();
      exit (1);
   }

   for (k = 0; k < n_local; k++) marked[k] = 0;
   if (!proc_id) index = 0;
   prime = 2;
   do {
      if (prime * prime > low_value)
         first = prime * prime - low_value;
      else {
         if (!(low_value % prime)) first = 0;
         else first = prime - (low_value % prime);
      }
      for (k = first; k < n_local; k += prime) marked[k] = 1;
      if (!proc_id) {
         while (marked[++index]);
         prime = index + 2;
      }
      if (n_proc > 1) MPI_Bcast (&prime,  1, MPI_INT, 0, MPI_COMM_WORLD);
   } while (prime * prime <= N);
   count = 0;
   for (k = 0; k < n_local; k++)
      if (!marked[k]) count++;
   if (n_proc > 1) MPI_Reduce (&count, &global_count, 1, MPI_INT, MPI_SUM,
      0, MPI_COMM_WORLD);

   /* Stop the timer */

   elapsed_time += MPI_Wtime();


   /* Print the results */

   if (!proc_id) {
      printf ("Time elapsed with %d processors: %12.6e seconds.\n",
         n_proc, elapsed_time);
      printf ("%d primes found below N=%d\n", global_count, N);
   }
   MPI_Finalize ();
   return 0;
}
