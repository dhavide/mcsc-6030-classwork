import sys
import numpy as np

N = int(sys.argv[1]) # find all primes up to N

from mpi4py import MPI
comm = MPI.COMM_WORLD
proc_id = comm.Get_rank()
n_proc = comm.Get_size()

# Error-checking: need N suficiently large relative to n_proc
if n_proc > np.sqrt(N):
    if proc_id==0:
        print "Too many processes."
    MPI.Finalize()
    sys.exit()

comm.Barrier()

# Arrays needed for for timing routines
local_time = np.array([-MPI.Wtime()],dtype=np.float)
time = np.array([0.0],dtype=np.float)

# Block allocation
lo_val = np.floor(proc_id*N/n_proc)
hi_val = np.floor((proc_id+1)*N/n_proc)-1
n_local = hi_val-lo_val+1
count_global = np.array([0])

# Note: is_prime is a local boolean array on each process
is_prime = np.ones(n_local,dtype=np.bool)
if proc_id==0:
   is_prime[0:2]=False # 0 & 1 are not considered prime numbers
   index = 2

# numpy array prime is needed for MPI broadcast below
k = 2
prime = np.array([k])

while k*k<=N:
    if (k*k > lo_val):
        first = k*k-lo_val
    else:
        r = lo_val%k
        if r==0:
            first = 0
        else:
            first = k - r
    is_prime[first::k] = False
    if proc_id==0: # Identify next prime number to broadcast
        index += 1
        while is_prime[index]==False:
            index += 1
        k = index # k is next prime to test
        prime[0] = k # copy into numpy array for broadcast
    comm.Bcast(prime,0)
    k = prime[0]

# At this point, all primes are found and can be counted
count = is_prime.sum()
comm.Reduce(count,count_global,MPI.SUM,0)

comm.Barrier()
local_time[0] += MPI.Wtime()
comm.Reduce(local_time,time,MPI.MAX,0) # Get worst timing

if proc_id==0:
    print "Time elapsed with %d processors: %12.6e seconds." % (n_proc,time[0])
    print "%d primes found below N=%d" % (count_global[0],N)
