# pass_Ring.py
# Simple Python program to ilustrate sends and receives in MPI

# Initialisation
import numpy as np
from mpi4py import MPI
comm = MPI.COMM_WORLD
proc_id = comm.Get_rank()
n_proc = comm.Get_size()

# Each process generates N random numbers
N = 10000
data = np.random.rand(N) + proc_id
print "Process %d drew data[0] = %g" % (proc_id, data[0])

# Now generate sends and receives:
dest = (proc_id + 1) % n_proc
src = (proc_id - 1) % n_proc
# To do receives, we need a buffer for receives
recv_data = np.zeros(N,dtype=np.float64)

# Let's do the sends and receives...
if proc_id % 2 ==0:
  comm.Send(data, dest=dest)
  print "Process %d before Recv has %g" % (proc_id, recv_data[0])
  comm.Recv(recv_data, source=src)
  print "Process %d after Recv has %g" % (proc_id, recv_data[0])
else:
  print "Process %d before Recv has %g" % (proc_id, recv_data[0])
  comm.Recv(recv_data, source=src)
  print "Process %d after Recv has %g" % (proc_id, recv_data[0])
  comm.Send(data, dest=dest)
