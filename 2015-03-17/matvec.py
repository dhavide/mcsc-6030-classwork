from mpi4py import MPI
import numpy as np

# This program illustrate master-slave distribution of work to compute a
# matrix-vector product of 1000x1000 matrix with 1000x1 vector
# Note: The matrix and vector are generated randomly in memory by master
#       process and verified using numpy.dot(). This is not that useful.

def master_process(n_rows, n_cols, comm):
    # Master process does the following:
    # 0. Allocate space for arrays as needed to compute y = A*x
    x = np.empty(n_cols,dtype=np.float)
    A = np.empty((n_rows,n_cols),dtype=np.float)
    y = np.empty(n_rows,dtype=np.float)
    ans = np.empty(1,dtype=np.float) # Required to receive entries of y

    # 1. Define data A (n_rows x n_cols matrix) and x (n_cols x 1 vector)
    x[:] = range(1,n_cols+1)
    for k in range(0,n_rows):
        A[k,:] = k+1

    # 2. Broadcast vector x to all processes
    comm.Bcast(x,master)

    # 3. Send first rows to other processes
    n_sent = 0
    for k in range(1,min(n_rows+1,n_proc)):
        comm.Send(A[n_sent,:],k,tag=n_sent) # Send row of matrix to process k
        n_sent += 1
    # Now loop over remaining rows, receiving dot products computed
    for k in range(n_rows):
        my_status = MPI.Status() # Create reference to MPI Status object
        # Receive a computed inner product from one of the processes
        comm.Recv(ans, source=MPI.ANY_SOURCE, tag=MPI.ANY_TAG, \
                  status=my_status)
        sender = my_status.source
        row = my_status.tag # The tag tells us which row is received
        y[row] = ans[0] # Copy message received into correct location
        # Either send another row back to sender or send final y=Ax
        # along with a tag to signal completion.
        if n_sent<n_rows:
            comm.Send(A[n_sent,:], sender, tag=n_sent)
            n_sent += 1
        else: # Inform sender that there is no more work
            comm.Send(y, sender, tag=n_rows+1) # Use tag=n_rows+1 to signal termination
    print "Finished!"
    print "Check: ||y-Ax|| = ", np.linalg.norm(y-np.dot(A,x))

def slave_process(proc_id, n_rows, n_cols, comm):
    # Allocate numpy arrays for receiving and sending rows & dot products
    x = np.empty(n_cols,dtype=np.float)
    my_row = np.empty(n_cols,dtype=np.float)
    ans = np.empty(1,dtype=np.float)
    # 1. Receive vector x from master process
    comm.Bcast(x,master)

    my_status = MPI.Status() # Create reference to MPI Status object
    comm.Recv( my_row, source=master, tag=MPI.ANY_TAG, status=my_status )
    row = my_status.tag # The tag tells us which row is received
    while row<n_rows:
        # Compute inner product and return result to master process
        ans[0] = np.dot(my_row,x)
        comm.Send(ans, master, tag=row)
        # Receive new row from master
        my_status = MPI.Status()
        comm.Recv(my_row, source=master, tag=MPI.ANY_TAG, status=my_status)
        row = my_status.tag

# Main program:
# Initialize MPI environment
world = MPI.COMM_WORLD
n_proc = world.Get_size()

if (n_proc==1):
    print "Error: requires two or more processes (no work will be done)."
else:
    master = 0 # proc_id of master process
    n_rows = 4
    n_cols = 1000
    proc_id = world.Get_rank()
    
    if proc_id==master:
        master_process( n_rows, n_cols, world )      
    elif proc_id<n_rows+1:
        # The conditional above prevents processing when there are more
        # processes than rows of the matrix (i.e., extra processes idle).
        slave_process( proc_id, n_rows, n_cols, world )
    else:
		pass
