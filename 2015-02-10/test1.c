#include <stdio.h>
#include <unistd.h>
int main (int argc, char *argv[])
{
  int thread_num, nthreads;
  #ifdef _OPENMP
  omp_set_num_threads (8);	/* specify number of threads */
  #endif
  printf ("Testing openmp, you should see each thread print...\n");
  #pragma omp parallel private(thread_num)
  {
    thread_num = omp_get_thread_num ();
    sleep (1);
    #pragma omp critical
    {
      #ifdef _OPENMP
      /* unique for each thread */
      printf ("This thread = %d\n", thread_num);
      #endif
    }
  }
  return 0;
}
