#include <stdio.h>
int main (int argc, char *argv[])
{
  printf ("This statement always prints.\n");
  #ifdef _OPENMP
  printf ("This statement prints only when compiled with \"-fopenmp\"\n");
  #endif
  return 0;
}
