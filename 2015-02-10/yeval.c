/*
   If this code gives a Segmentation Fault when compiled and run with -fopenmp 
   Then you could try:
      $ ulimit -s unlimited
   to increase the allowed stack size.
   This may not work on all computers.  On a Mac the best you can do is
      $ ulimit -s hard
*/

#include <stdio.h>
#include <omp.h>
#include <math.h>
#include <stdlib.h>

int
main (int argc, char *argv[])
{
  int n = 100000000; /* n = 10**8 */
  int k, nthreads;
  double y[n], dx, x;
  char *err;
  /* Specify number of threads to use: */
  #ifdef _OPENMP
  nthreads = (int) strtol (argv[1], &err, 10);	/* No error-checking... */
  omp_set_num_threads (nthreads);
  printf ("Using OpenMP with %d threads\n", nthreads);
  #endif
  dx = 1.0 / (n + 1.0);
  #pragma omp parallel for private(x)
  for (k = 0; k < n; k++)
    {
      x = k * dx;
      y[k] = exp (x) * cos (x) * sin (x) * sqrt (5 * x + 6.0);
    }

  printf ("Filled vector y of length %d\n", n);
  return 0;
}
