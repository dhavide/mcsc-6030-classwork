{
 "metadata": {
  "name": ""
 },
 "nbformat": 3,
 "nbformat_minor": 0,
 "worksheets": [
  {
   "cells": [
    {
     "cell_type": "heading",
     "level": 1,
     "metadata": {},
     "source": [
      "An application of scalar root-finding using ``scipy.optimize`` routines"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Consider a mechanism comprising four planar rigid rods of lengths $a_1$, $a_2$, $a_3$, and $a_4$. Assume the rod of length $a_1$ is anchored on the horizontal axis with the ends anchored at $(0,0)$ and $(a_1,0)$. Assume further that the remaining rods are connected as illustrated here:\n",
      "\n",
      "<img src=\"files/img/rods-01.svg\" alt=\"4-link mechanical linkage\" />&nbsp;<img src=\"files/img/rods-02.svg\" alt=\"4-link mechanical linkage\" />\n",
      "\n",
      "The *Freudenstein equation* \n",
      "\\begin{equation} \\frac{a_{1}}{a_{2}} \\cos(\\beta) - \\frac{a_{1}}{a_{4}} \\cos(\\alpha) - \\cos(\\beta-\\alpha) =\n",
      "      - \\frac{a_{1}^{2} + a_{2}^{2} - a_{3}^{2} + a_{4}^{2} }{2a_{2}a_{4}} \\end{equation}\n",
      "describes the kinematics of these four rigid planar rods. If one of the angles, say, $\\beta$ and all the rod lengths $a_{k}$ ($k=1,\\ldots,4$) are prescribed, determining the geometry of the 4-link frame reduces to solving the equation above for the remaining unknown parameter $\\alpha$.\n",
      "\n",
      "To implement this in Python, let us first create a *lambda function* function ``f_freudenstein`` that can be used in conjunction with a zero-finding routine. Notice the equation is rearranged so that the right-hand side is zero (as would be required for a zero-finding routine)."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "import numpy as np\n",
      "f_freudenstein = lambda alpha, beta, A1, A2, A3, A4 : (A1/A2)*np.cos(beta) - (A1/A4) * np.cos(alpha) \\\n",
      "          - np.cos(beta-alpha) + (A1**2+A2**2-A3**2+A4**2)/(2*A2*A4)\n",
      "print f_freudenstein(0.1, 0.25,11,13,8,10)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "-0.983775243214\n"
       ]
      }
     ],
     "prompt_number": 1
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Since the solver ``scipy.optimize.root`` expects a single-variable function as an input, one strategy to use ``f_freudenstein`` to find the angle ``alpha`` is to define a temporary, single-variable lambda function using fixed constants ``beta``, ``a1``, ``a2``, ``a3``, and ``a4`` as inputs (this is called *currying* in functional programming)."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "a1 = 10; a2 = 13; a3 = 8; a4 = 10;\n",
      "beta = 0.1\n",
      "f = lambda x : f_freudenstein(x,beta,a1,a2,a3,a4) # f is a function of a *single* variable\n",
      "print f(0.5)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "-0.798643555893\n"
       ]
      }
     ],
     "prompt_number": 2
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "With the single-variable function ``f`` thus defined, we can invoke the function ``root`` from the ``scipy.optimize`` module to find the corresponding solution $\\alpha$. Notice that we have to provide an initial iterate ``alpha_init`` as well as the function ``f`` as input to ``scipy.optimize.root``; the other input arguments assume default values (we can in principle choose values for these also)."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "import scipy.optimize\n",
      "alpha_init = 1.1\n",
      "alpha_sol = scipy.optimize.root(f,alpha_init)\n",
      "print alpha_sol\n",
      "alpha = alpha_sol['x'] # Extract the actual angle alpha from the dictionary alpha_sol"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "  status: 1\n",
        " success: True\n",
        "     qtf: array([ -6.27506191e-09])\n",
        "    nfev: 5\n",
        "       r: array([-1.72916584])\n",
        "     fun: array([  6.66133815e-15])\n",
        "       x: array([ 1.09647496])\n",
        " message: 'The solution converged.'\n",
        "    fjac: array([[-1.]])\n"
       ]
      }
     ],
     "prompt_number": 3
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "It is often the case that one wants to solve the same equation with different parameters (i.e., different rod lengths or different angle $\\beta$ in this case). As this scenario arises so often, ``scipy.optimize.root`` can accept the multi-variable function ``f_freudenstein`` as an input parameter without currying. The remaining input parameters need to be passed to ``scipy.optimize.root`` in a Python tuple using the keyword argument ``args``."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "a1 = 10; a2 = 13; a3 = 8; a4 = 10;\n",
      "beta = 0.2\n",
      "alpha_init = 1.2\n",
      "alpha_sol = scipy.optimize.root( f_freudenstein, alpha_init, args=(beta,a1,a2,a3,a4) )\n",
      "print alpha_sol\n",
      "alpha = alpha_sol['x']"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "  status: 1\n",
        " success: True\n",
        "     qtf: array([ -5.15632426e-09])\n",
        "    nfev: 6\n",
        "       r: array([-1.72051006])\n",
        "     fun: array([  1.89848137e-14])\n",
        "       x: array([ 1.14429629])\n",
        " message: 'The solution converged.'\n",
        "    fjac: array([[-1.]])\n"
       ]
      }
     ],
     "prompt_number": 4
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Since we can solve the Freudenstein equation with fixed length rods and a given angle $\\beta$, now we can embed the workflow above inside a loop to determine a continuous set of configurations of the mechanism. To do so, write a function with the solution procedure just discovered embedded. That function can then be called within a loop."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "def solve_freudenstein(alpha_init, beta, A1, A2, A3, A4):\n",
      "    f = lambda x: f_freudenstein(x,beta,A1,A2,A3,A4)\n",
      "    alpha_sol = scipy.optimize.root(f,alpha_init)\n",
      "    return alpha_sol['x']\n",
      "\n",
      "N_beta = 50 # Increase for better resolution\n",
      "beta_vals = np.pi * np.linspace(0, 0.6, N_beta)\n",
      "alpha_lo = np.zeros_like(beta_vals)\n",
      "alpha_hi = alpha_lo.copy()\n",
      "\n",
      "a1 = 10; a2 = 13; a3 = 8; a4 = 10; # Fix rod lengths\n",
      "# Start by determining the two values of alpha for the largest beta\n",
      "beta = beta_vals[0]\n",
      "alpha_init = -0.1\n",
      "alpha_lo[0] = solve_freudenstein(alpha_init,beta,a1,a2,a3,a4)\n",
      "alpha_init = 2*np.pi/3\n",
      "alpha_hi[0] = solve_freudenstein(alpha_init,beta,a1,a2,a3,a4)\n",
      "\n",
      "for k in range(1,N_beta): \n",
      "    beta = beta_vals[k]\n",
      "    alpha_init = alpha_lo[k-1] # Use the last point as an initial iterate\n",
      "    alpha_lo[k] = solve_freudenstein(alpha_init,beta,a1,a2,a3,a4)\n",
      "    alpha_init = alpha_hi[k-1]\n",
      "    alpha_hi[k] = solve_freudenstein(alpha_init,beta,a1,a2,a3,a4)\n"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [],
     "prompt_number": 5
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "import matplotlib.pyplot as mpl\n",
      "%matplotlib inline\n",
      "mpl.plot(beta_vals, alpha_lo,'b-o',beta_vals,alpha_hi,'r-o')\n",
      "mpl.show()"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "metadata": {},
       "output_type": "display_data",
       "png": "iVBORw0KGgoAAAANSUhEUgAAAXIAAAD/CAYAAADsfV27AAAABHNCSVQICAgIfAhkiAAAAAlwSFlz\nAAALEgAACxIB0t1+/AAAGXpJREFUeJzt3XtsVNeBx/HfeGxsY2NjZ2lKwkJZk0B3MQ3bggUEOqYJ\nINEoLRU1pOYRuaihK2KRbFdJWIshYCCs0vBoEkUbC6JQZVFUEpXSdh0tGVCg5EETKEp4uSEbEpGo\n1PiF33P3jylezL138L0ej33M9yON5DnH58yZO3d+Pj733pmAZVmWAADGSunvAQAAeocgBwDDEeQA\nYDiCHAAMR5ADgOEIcgAwnK8gb29v15IlSzRr1iwVFRVp3759iR4XAKCHUv00+uUvf6kRI0bo5Zdf\nVm1tre666y7dd999iR4bAKAHAn4uCGpqapJlWcrOztalS5c0depU1dTU9MX4AAA34GtGnpWVJUlq\naGjQwoULVVlZmdBBAQB6zvfBzk8//VSzZ8/W0qVLtWjRokSOCQDgheXDxYsXrQkTJlgHDhxw/Z2C\nggJLEjdu3Lhx83ArKCjwnMm+1sjLy8v16quvavz48V1lv/vd75SRkdF1PxAIiM/jSoxwOKxwONzf\nwxg02J6JxfZMLD/Z6WuNfNu2bdq2bZufpgCABOOCIAAwHEFugFAo1N9DGFTYnonF9ux/vtbIe9Qx\na+QA4Jmf7GRGDgCGI8gBwHAEOQAYztfphwD636H9+1W9fbtSW1vVkZ6uOQ8/rFnz57uW+2mTyL4G\n8phNR5ADCZassPrv8nJVXvNhdWtqanTy3Xf12e7dtvKrvLRJZF/JauOnr6tMDn/OWgGUuFmfY8AW\nFOj20lJ7wBQUaO7fLqzz1GbjRlU/95w2HDxoex4lOTnaU19vK68YP15WNKoNZ8/a26Sna09rq708\nNVV7OjrsfWVny7IsbWhqsrcJBLTH4X1fkZoqS9IGh/5KJO2xlUoVmZmyAgFtuHKlx2Nzey4VEyfK\nSknRhhMnbHU//sY3dGtjo6fX5mpdXwR80q7sBPpbf85uXWd9zc2qfvbZbuWSVFlTo5Knn9aexkZb\necXKlbI6O1X5+ef2NuvXa080am+zeLFSU5wPb2U6hKskBRsbpUDAuY1b+ZAhkkNYBsePj/X13nv2\nNtnZUkODvc2UKbE2R47Y2wwb5txm4sTYD+++2+OxuT2X4JdfStdty6sajx/Xi9eVVdbUqKKsTFZa\nmiovXLDV/fixx3Rrc7Pj7L4/ZusEOZImUUsOUvx/kXscymfOSJ9/ruqdO53Dd8sW7WlutpdXVtpm\ng5U1NaooKXEP2JYWx/Jgerrk1mboUOm68Jek4MyZ6khPl6qrbXXNw4dLly7ZyjsnTozN8q4LJUlq\nzsqSHMbXnJkpOcyGO//u71xnjM1DhjiWd+bkeG+Tn+/exmVsbs+lc/LkWF8O2yw9L0+qrbWVB/Pz\npfZ2x8dvPHnSOfx37CDIYY5+W9M9e1a1Q4fqOYfgrXjkEVkdHar8859tdY6z2/PnVVFe7h6+nZ3O\n5enpzjPVeAGbm+scsAUFsYA5c8beJj3dMcg7MzI0Z9Uqramp6bZtnigo0LdLS7Xmuu35REGB5q1a\nJUme2iSyr2S18dNXVk6OY5B3jh4de23OnbPVuYa/yx/svkaQ30T6ekYcL5Srt23ztuSwbJms9nZV\nXrfeW/nxx1rm9u/zlStSMOhY5zq7nTrVPXyHDXMM3+aMDMlhCSNZATtv1aqu16dixw4FW1rUmZHR\nVX5oyhTH8qu8tElkX8lq47UvSVpz3f7sO/yv+QTYZOJgp6H67SDc6NGqzczUc6dP28bkeqApJUXB\naFRhh+exPDVVuxxmt+HCQikYVPiDD2x1i/Ly9F8Ob6KKuXNjB+EcQrnkllu0xyGUK+bO1b2rVtme\n5xMFBRrlsG3ilc/btq1rW79xTVjcezVgXMol+WqDxPH62kj298a1+0Bv+MlOgnwASEYoV2/f7hxw\n2dm2GbEkVeTlyWpv1waHumUpKXrJ4cDR8owM7XL41zI8Y4Y6srI8B6xbKP948mTdWl/v+CaSnN9g\niQ5fAhZ9tQ8Q5ANAQpYp4oXyM8+oeutWbThwwPbYJUOHao/DAaCK9HQF29oUdng9XGfEkyZJaWkK\nHztmq3ObESdy1hsvlK/WMbvFYESQJ1iiQtl1mWLs2NgyxYcf2h67JCNDexxmtxWSgikpCjvNiIcM\n0a62Nlt5uKgoNiN2Cv8EzoiTueQADFYEeRx9HspjxsRC+dQp22NXjBwZW6b4y19sdcsCAb3kNFN2\nW6aYOVMdmZl9vg7sd0ZM8AK9c9NcENTn5x2fOqXajAznU9zKymS1tanyuqWFyk8+0TKX09iCOTlS\nWprkEOStw4c7Hv12PR926FBfZ0Yk+iwHyfnCh1nz5xPcQJINiBm5l4N9kseZcpyzLCq+8hVZbW3a\ncPmyrc7tgF7461+PrR07XOrr52yKZC5TABj4+mVp5e2339Zjjz2mN9980zaYNXPmJPYMjHjLF7fe\nGgtlhyB1DeV//MdYKB8/bqtLZCizTAGgp5K+tLJlyxbt3r1b2dnZjvUbqqvdLxQ5e1a6eFHV//mf\nzheKbNyoPdddHht3+SI/P7Z84RC+rbm5zifv//3fu26w7K99TWvy8z1dJLB0/XpJ7ksR8epYpgDg\nV69m5Hv37tWkSZO0ZMkS/eEPf+jecSCgqx2XDBmiPQ5nU1RkZCiYmqqww7nKy4cO1S6HU+kGwkxZ\n4mwKAH0j6TPyBQsW6Pz58zf8vUyXy6aDRUXul0e7fCjOQJgpXy0nuAEMBEk5a6V56FDpuk+Rk/x9\nNgWhDADd9WmQhyX9T16eciZOVOnZs9p9zWcu9/a0OEIZwGAQiUQUiUR61Uevz1o5f/68HnjgAR25\n7gPjA4GA/v1vF6JwBgYA9AxXdgKA4fxkp/O5fAAAYxDkAGA4ghwADEeQA4DhCHIAMBxBDgCGI8gB\nwHAEOQAYjiAHAMMR5ABgOIIcAAxHkAOA4QhyADAcQQ4AhiPIAcBwBDkAGI4gBwDDEeQAYDiCHAAM\nR5ADgOF8BXk0GtVDDz2k6dOnq7i4WDU1NYkeFwCgh3wF+euvv662tjYdOXJEmzdv1qOPPprocQEA\neshXkB8+fFjz5s2TJBUVFem9995L6KAAAD3nK8jr6+uVk5PTdT8YDCoajSZsUACAnkv10ygnJ0cN\nDQ1d96PRqFJS7H8TwuFw18+hUEihUMjPwwHAoBWJRBSJRHrVR8CyLMtro71792rfvn3auXOnjh49\nqvXr12v//v3dOw4E5KNrALip+clOX0FuWZZ++tOf6sSJE5KknTt36s477+z1YADgZpe0IO+rwQDA\nzc5PdnJBEAAYjiAHAMMR5ABgOIIcAAxHkAOA4QhyADAcQQ4AhiPIAcBwBDkAGI4gBwDDEeQAYDiC\nHAAMR5ADgOEIcgAwHEEOAIYjyAHAcAQ5ABiOIAcAwxHkAGA4ghwADNfrIH/ttdf0ox/9KBFjAQD4\nkNqbxuXl5aqurtbkyZMTNR4AgEe9mpHPmDFDzz//vCzLStR4AAAe9SjIq6qqVFhY2O127Ngx/fCH\nP+zr8QEAbqBHSytlZWUqKyvz3Hk4HO76ORQKKRQKee4DAAazSCSiSCTSqz4CVi/XRSKRiF544QW9\n8sor3TsOBFhyAQCP/GRnr89aCQQCCgQCve0GAOBTr2fkrh0zIwcAz/plRg4A6F8EOQAYjiAHAMMR\n5ABgOIIcAAxHkAOA4QhyADAcQQ4AhiPIAcBwBDkAGI4gBwDDEeQAYDiCHAAMR5ADgOEIcgAwHEEO\nAIYjyAHAcAQ5ABiOIAcAwxHkAGA4z0FeV1en++67T6FQSNOnT9fRo0f7YlwAgB4KWB6/rjkcDis/\nP18PP/ywzpw5o8WLF+vYsWP2jn18EzQA3Oz8ZGeq1wdZvXq10tPTJUnt7e3KzMz02gUAIIHiLq1U\nVVWpsLCw2+3cuXPKyMjQxYsXtWTJEm3atClZYwUAOPC8tCJJf/rTn7R48WI9/fTTmjt3rnPHgYDW\nrl3bdT8UCikUCvkeKAAMRpFIRJFIpOv+unXrPC+teA7yDz/8UAsWLNCrr76qwsJC945ZIwcAz/xk\np+cg/973vqcTJ05ozJgxkqThw4frtddeS8hgAOBml5Qg78vBAMDNzk92ckEQABiOIAcAwxHkAGA4\nghwADEeQA4DhCHIAMBxBDgCGI8gBwHAEOQAYjiAHAMMR5ABgOIIcAAxHkAOA4QhyADAcQQ4AhiPI\nAcBwBDkAGI4gBwDDEeQAYDiCHAAMl+q1QVNTkx544AFdvnxZQ4YM0UsvvaTbbrutL8YGAOgBzzPy\nF198UVOmTNHBgwdVWlqqLVu29MW4AAA95HlGXl5ermg0Kkn65JNPlJeXl/BBAQB6Lm6QV1VVaevW\nrd3Kdu3apW9+85v6zne+o5MnT6q6urpPBwgAiC9gWZblt/Hp06c1f/58nTt3zt5xIKC1a9d23Q+F\nQgqFQn4fCgAGpUgkokgk0nV/3bp18hrLnoN806ZNGjVqlJYsWaILFy7onnvu0alTp+wdBwKeBwMA\nNzs/2ek5yL/88kstW7ZMLS0t6uzs1FNPPaVp06YlZDAAcLNLSpD35WAA4GbnJzu5IAgADEeQA4Dh\nCHIAMBxBDgCGI8gBwHAEOQAYjiAHAMMR5ABgOIIcAAxHkAOA4QhyADAcQQ4AhiPIAcBwBDkAGI4g\nBwDDEeQAYDiCHAAMR5ADgOEIcgAwHEEOAIbzHeSnTp3S8OHD1dbWlsjxAAA88hXk9fX1evTRR5WR\nkZHo8QAAPPIc5JZl6Sc/+Yk2bdqkzMzMvhgTAMCD1HiVVVVV2rp1a7eyMWPGaNGiRZo0aZKkWLAD\nAPpPwPKYxHfccYdGjRolSTp69KiKiooUiUTsHQcCWrt2bdf9UCikUCjUq8ECwGATiUS6Zei6des8\nT5A9B/m1xo4dq9OnT2vIkCH2jgMBZusA4JGf7OzV6YeBQKA3zQEACdCrGXncjpmRA4BnSZ+RAwD6\nH0EOAIYjyAHAcAQ5ABiOIAcAwxHkAGA4ghwADEeQA4DhCHIAMBxBDgCGI8gBwHAEOQAYjiAHAMPF\n/YYgAObZv/+Qtm+vVmtrqtLTO/Tww3M0f/6suHVey/30law28foarAhyIEmSEXCSVF7+36qpqex6\n3JqaNV0/O9W9++5J7d79WY/L/fSVrDbx+kr0H6yBhM8jB+Loy4AtKFijbdvmutaVlt5uC6UbtcnJ\nqdX77z9nex4zZ1aos9PSkSMbbHXDhpWooWGPrXzo0BJduWIv/4d/qFA0aun8eXtf6eklam21t7nt\ntgpJlj7/vOdtxoyJtfnkE3sbt7G5PZfZsyv0L/9yr/7t3xKzna/W9UXA+8lOZuQYdPp6dutnBpmd\nXauamu4BW1NTqbVrK9TRYXX7/at1Tz1VopaWPbbyJUtioVxfb28jLXPcJu+8E1SKyxGxK1cyHcvb\n253Lo9Ggc0eSAgHnNtnZ3tsEg0G55VlHh3Mbt+dy8GBQb75ZLcuyb7PKyhJ1dNi38+OPVyg11fm1\nqaj4serrb3Wd+ScbQY5+l+j10USE76lTazR0qHP4/sd/2GeDNTWVWro0FspeAvbPf3YPWMtyDqXR\no2Nt3n/fXjd8eKsuX7aXh0KdsixL1dVObZp16ZK9PCfHuXz8+Fhf58/b67KymtXSYi8fOzbW5syZ\nnre5445Ym48/ttcNG+Y8Nrfncs89nWppSdXBg/a6IUMy1dFhL794MehYLknHjzcqGn2xW1lNTaV2\n7KggyGGOgbame+bMGl24IO3cWe1pdltSEgvf1tbubf73fyuVkuIcvm6zwVGjggoGnQM2L69VtbX2\n8qlT3QM2O7tZra328q9+tdP1X++xY7NVX7/muqWAJ7Rq1TxJse13fV1p6be1e3fPy/30law28fra\nvt1hI0vKzGzWlSv28n/+Z/fXJisrXQ0N9vKWFvf/PPoSQY5+XXJwm/X+67+6LzmsX1+iaLR7KJ8/\nX6lHHqlQSorzLu02u/3612Ph+/bb9rrcXOfwdZsNjhzpHrBf+1q28vP7PuDWr18qSdqxo0ItLUFl\nZHRq1ap53WaJTnVTphzyVO6nr2S1iddXorZzTk6W4x/sjIxOx9e/r3k+2GlZlkaNGqU777xTkjRt\n2jRt3LjR3jEHO/tFMg7CuR1Qy8kpUX29/UDTV78aC+W//MV+0Cq25PCSrXTkyLBSU6VPPw3b6rKz\nl6uxcZet/NvfDis9vUPV1fbHueWWEl26ZB/b3LkVf5t12dtMnmxfB4298Uc5HBx7Qtu2xd749m32\n/3U7drxxTcDc2+21capzK4/XBu4StZ0l99e5t6+Bn+z0HOTnzp3TI488ol//+tcJH8zNqj/PjMjK\nqtWJE/ZQvvPOCrW3W/r4456HbzC4XJ2du2zlEybEQvnkybCtLi9vkWpr/8tWHi9g44XyqlX3Or7B\nEh2+BCz66nVOylkrx44d02effabZs2crMzNTzzzzTNfsHP23THH69BplZjovU6xcWaG2NktffNHz\ng3BNTUGluuwdubmtqquzl7sdaBozJrbkcPKkvS7RSw5Xt3Wi/n2XnM9CmD9/luubNl4dBo+B9DrH\nnZFXVVVp69at3cqee+45ffnll/rBD36gw4cPa/Xq1XrnnXfsHQ+CGflAXKYYMSI2U7582T5TTUlZ\npmjUPlMeNy42Iz51Kmyr8zMjHuhLDoDJkrK00tzcrNTUVKWlpUmSRo0apQsXLjgOZu3atV33Q6GQ\nQqGQp8ElUn+G8je+EZsRf/RRz8PXbZnin/4pFsrHj4dtdYkMZZYcgOSIRCKKRCJd99etW9f3Qf74\n448rPz9fP/vZz3T8+HGtXLlSR44csXfchzPy/gzliRNjM+LTp+2hGAgsk2XZQ/mWW8JKS5MuXgzb\n6nJyFqm+3h6+iTw41xczYgB9Iykz8rq6OpWWlqqxsVGpqal69tlnHdfIvQzGy8E+KbGh3NZm6cyZ\nnh/Q+8pXYqH82WdhW51bKA/0ZQoAA0dSgtzLYObMWePrSrx4n3/gFsqFhbFQdpop+wnl3NxFqqtj\nmQJAcg24IJesG34ozfbt1Y6hmJVVoqYm+9KC2/LFiBGxUP7887CtbiCEskT4ArixARnkkpSdXaLG\nRnsoZ2VVqLU1qI6OsK0uLW252tt32coTuXxBKAMYaAbspx+2tDhfHj1hQlDZ2R2OH2Tj9oE9BQXe\nP0vC72XLkvsnmQ2kc0gB3NySMiPviyvxmCkDGIwG5NLKjUI53kE9QhnAzWbABfncuf9OKAOABwMu\nyE2/RB8Aks1Pdrp8NwkAwBQEOQAYjiAHAMMR5ABgOIIcAAxHkAOA4QhyADAcQQ4AhiPIAcBwBDkA\nGI4gBwDDEeQAYDjPQd7Z2any8nLdfffdmjp1qn7/+9/3xbgAAD3kOchffvlldXR06K233tLrr7+u\njz76qC/GhWtEIpH+HsKgwvZMLLZn//Mc5NXV1br99tv13e9+VytWrND999/fF+PCNXijJBbbM7HY\nnv0v7nd2VlVVaevWrd3KRowYoczMTP3mN7/RoUOH9OCDD+qg05duAgCSIm6Ql5WVqaysrFvZ4sWL\nNX/+fEnSrFmzdObMmb4bHQDgxiyPfvGLX1hlZWWWZVnWBx98YBUVFTn+XkFBgaXYty9z48aNG7ce\n3goKCrzGsuX5q97a2tq0cuVKffjhh5Kk559/XnfddZeXLgAACdRn39kJAEgOLggCAMP1Ksij0age\neughTZ8+XcXFxaqpqelWv2/fPk2dOlXTp0/Xiy++2KuB3gxutD2feeYZTZw4UcXFxSouLuZAcw+8\n/fbbKi4utpWzb/rjtj3ZN71pb2/XkiVLNGvWLBUVFWnfvn3d6j3vn55X1a/xq1/9ynrwwQcty7Ks\no0ePWvfff39XXVtbmzVu3Djr8uXLVltbmzVlyhTriy++6M3DDXrxtqdlWVZpaan1xz/+sT+GZqSn\nnnrKKiwstKZNm9atnH3TH7ftaVnsm17t3LnTWr16tWVZlvXXv/7VGj16dFedn/2zVzPyw4cPa968\neZKkoqIivffee111H330kcaNG6fc3FylpaXp7rvv1qFDh3rzcINevO0pSceOHdPGjRs1c+ZMbd68\nuT+GaJRx48Zp7969sq47DMS+6Y/b9pTYN71auHChnnzySUmx/8RTU///THA/+2evgry+vl45OTld\n94PBoKLRaFddbm5uV92wYcNUV1fXm4cb9OJtTyl2Dv8LL7ygAwcO6K233tL+/fv7Y5jGWLBgQbc3\nyFXsm/64bU+JfdOrrKwsZWdnq6GhQQsXLlRlZWVXnZ/9s1dBnpOTo4aGhq770WhUKSmxLnNzc7vV\nNTQ0KC8vrzcPN+jF256SVF5ervz8fKWlpWn+/Pl6//33+2OYxmPfTDz2Te8+/fRTzZ49W0uXLtWi\nRYu6yv3sn70K8hkzZui3v/2tJOno0aOaNGlSV92ECRN09uxZ1dbWqq2tTYcOHdK0adN683CDXrzt\nWVdXp8LCQjU1NcmyLB04cEDf+ta3+muoRmPfTCz2Te+++OILzZkzR1u2bNHy5cu71fnZP+Neon8j\n3//+9/XGG29oxowZkqSdO3fqlVdeUWNjo1asWKGf//znmjt3rqLRqMrKyjRy5MjePNygd6PtuXnz\nZhUXFys9PV333HNP13o64gsEApLEvpkgTtuTfdObjRs3qq6uTk8++WTXWvmKFSvU1NTka//kgiAA\nMBwXBAGA4QhyADAcQQ4AhiPIAcBwBDkAGI4gBwDDEeQAYDiCHAAM938z6DfOkKEkRgAAAABJRU5E\nrkJggg==\n",
       "text": [
        "<matplotlib.figure.Figure at 0x7fd214050c90>"
       ]
      }
     ],
     "prompt_number": 7
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "This can be used, in principle, to generate an animation of the planar configurations of the rods."
     ]
    }
   ],
   "metadata": {}
  }
 ]
}