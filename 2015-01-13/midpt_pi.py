import sys

N = int(sys.argv[1]) # Reading number of rectangles from command-line
delta_x = 1/float(N)

# Main loop
pisum = 0.0
for k in range(1,N+1):
    xk = (k-0.5)*delta_x
    pisum += 4.0*delta_x/(1.0+xk*xk)

print pisum
