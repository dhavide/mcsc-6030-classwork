/* check-endian.c */
/* Toy program to determine whehter the native machine format is big- or
   little-endian */

/* Given a pointer to a memory location, the contents are printed
   as hexadecimal digits. */

#include <stdio.h>
void show_mem_rep(char *start, int n) 
{
    int i;
    for (i = 0; i < n; i++)
         printf(" %.2x", start[i]);
    printf("\n");
}
 
/*Main function to call above function for 0x01234567*/
int main()
{
   int i = 0x01234567; /* Create 4-byte integer, hex digits 01 23 45 67 */
   show_mem_rep((char *)&i, sizeof(i));
   /* Prints "67 45 23 01" if run on machine using little-endian storage */
   /* Prints "01 23 45 67" if run on machine using big-endian storage */
//   getchar();
   return 0;
}
