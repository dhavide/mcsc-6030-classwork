# wavedemo.py
import numpy as np
import matplotlib.pyplot as plt

F = lambda x: np.sin(2*np.pi*x)
G = lambda x: np.zeros_like(x)
Tmax = 10.0
m = 1000
n = 100

U = np.zeros((n+1,m+1))
h = 1.0/n
k = Tmax/m
L = (k/h)**2
x = np.linspace(0,1,n+1)
# Assign initial displacement data to time t=t[0]
U[1:-1,0] = F(x[1:-1])
# This next line uses velocity data to compute U @ t=t[1]
U[1:-1,1] = (L/2.0)*(U[2:,0] + U[0:-2,0]) + \
            (1.0-L)*U[1:-1,0] + k * G(x[1:-1]) 
# Ready now to begin time-stepping
for q in range(1,m):
    U[1:-1,q+1] = 2.0*(1.0-L)*U[1:-1,q] + L*(U[0:-2,q] + U[2:,q]) - U[1:-1,q-1]

# Display the results in an animation
# display results
fig = plt.figure()
plt.axis()
plt.xlim(0,1)
plt.ylim(-1,1)
for q in range(m):
   plt.cla()
   plt.plot(x,U[:,q],'b-')
   plt.xlim(0,1)
   plt.ylim(-1,1)
   plt.pause(0.03)
