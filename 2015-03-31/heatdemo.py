# heat1_demo.m
# Demonstration of explicit Euler discretisation for Heat equation
import numpy as np
import matplotlib as mpl
mpl.interactive(True)
import matplotlib.pyplot as plt


F = lambda x: 0.5 - np.abs(x-0.5)
Tmax = 2.0
n = 10;
m = 1000  # With m too small, stability violations creep in!
h = 1.0/n  # Spatial grid spacing
k = 1.0/m  # Temporal grid spacing
x = np.linspace(0,1,n+1)
t = np.linspace(0,Tmax,m+1)
u0 = F(x)  # Sampling the initial condition

U = np.zeros( (n+1,m+1))
U[:,0] = u0
L = k/h**2
# This is where the actual time-stepping gets done...
for q in range(1,m):
    U[1:-1,q] = (1-2*L)*U[1:-1,q-1] + L*(U[:-2,q-1]+U[2:,q-1])

# display results
fig = plt.figure()
plt.axis()
plt.xlim(0,1)
plt.ylim(0,1)
for q in range(m):
   plt.cla()
   plt.plot(x,U[:,q],'b-')
   plt.xlim(0,1)
   plt.ylim(0,1)
   plt.pause(0.03)
