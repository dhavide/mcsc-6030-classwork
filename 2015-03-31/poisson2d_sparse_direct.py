import sys
import numpy as np
import matplotlib.pyplot as plt
import scipy.sparse as ss         # Needed for construction of sparse linear system
import scipy.sparse.linalg as ssl # Needed for solution of sparse linear system

if len(sys.argv)==2:
   n = int(sys.argv[1])
else:
   print sys.argv[0], ": expected command line argument n"
   sys.exit()

# Problem setup
h = 1.0/(n+1)
x = np.linspace(0,1,n+2)
(x,y) = np.meshgrid(x,x)
# f(x,y) is RHS when exact solution is u(x)= sin(2*pi*x)*sin(pi*y)
f = lambda x,y: 5*(np.pi)**2*np.sin(np.pi*x)*np.sin(2*np.pi*y)
# Observe: RHS vector must be scaled by h**2 for correct solution
#          f must be sampled at interior points only.
#          f must be flattened into a 1D vector for use with matrix solver
F_RHS = (h**2)*f(x[1:-1,1:-1],y[1:-1,1:-1])
F_RHS = F_RHS.flatten()

# Now solve the actual equations using sparse linear solver
# Construct the coefficient matrix explicitly
Tn = ss.spdiags( [ n*[-1], n*[2], n*[-1] ], [-1,0,1], n, n)
In = ss.eye(n,n);
Tnxn = ss.kron(Tn,In) + ss.kron(In,Tn)
Tnxn = Tnxn.asformat('csr')  # csr->Compressed Sparse Row (eliminates solver warnings)
print "Solving the sparse linear system of equations: n =", n, ", n**2 =", n**2
# Call the sparse solver
U = ssl.spsolve(Tnxn,F_RHS)

# Compute error of computed solution
U_exact = np.sin(np.pi*x)*np.sin(2*np.pi*y)
err = U_exact[1:-1,1:-1].flatten()-U
print "Error in computed solution: ", np.linalg.norm(err)

# Visualise exact solution
plt.contourf(x,y,U_exact,10)
plt.colorbar()
plt.show()
