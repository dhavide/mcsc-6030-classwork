import sys
import numpy as np
import matplotlib.pyplot as plt
import scipy.sparse as ss         # Needed for construction of sparse linear system
import scipy.sparse.linalg as ssl # Needed for solution of sparse linear system

if len(sys.argv)==2:
   n = int(sys.argv[1])
else:
   print sys.argv[0], ": expected command line argument n"
   sys.exit()

# Problem setup
h = 1.0/(n+1)
x = np.linspace(0,1,n+2)
# f(x) is the RHS when exact solution is u(x)= sin(2*pi*x)
f = lambda x: (2*np.pi)**2*np.sin(2*np.pi*x) 
# Observe: RHS vector must be scaled by h**2 for correct solution
#          also, f must be sampled at interior points only.
F_RHS = (h**2)*f(x[1:-1])

# Now solve the actual equations using a sparse linear solver
# Construct the coefficient matrix explicitly
Tn = ss.spdiags( [ n*[-1], n*[2], n*[-1] ], [-1,0,1], n, n)
Tn = Tn.asformat('csr')  # csr->Compressed Sparse Row (eliminates solver warnings)
print "Solving the sparse linear system of equations: n =", n
# Call the sparse solver
U = ssl.spsolve(Tn,F_RHS)

#Compute error in solution
U_exact = np.sin(2*np.pi*x[1:-1])
err = U-U_exact
print "Error in computed solution: ", np.linalg.norm(err)

# Having obtained the solution, visualise it
U = np.hstack( ([0], U, [0] ) ) # Append boundary data to vector computed
plt.plot(x,U)
plt.xlim(0,1)
plt.ylim(-1,1)
plt.show()
