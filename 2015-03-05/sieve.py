import sys
import numpy as np

N = int(sys.argv[1])
is_prime = np.ones(N+1,dtype=np.bool)
is_prime[0] = False # 0 is not a prime number
is_prime[1] = False # 1 isn't either.

KMAX = np.ceil(np.sqrt(N))
k=2 # First prime to try
while (k<=KMAX):
    if is_prime[k]:
 	 is_prime[k**2::k] = False # mark multiples of k
    k+=1

result = np.nonzero(is_prime)[0]
print "%d primes found up to %d." % (len(result),N)
