/*
 *   Circuit Satisfiability, Version 2
 *
 *   This enhanced version of the program prints the
 *   total number of solutions.
 */

#include "mpi.h"
#include <stdio.h>

int main (int argc, char *argv[]) {
   int count;            /* Solutions found by this proc */
   int global_count;     /* Total number of solutions */
   int k;
   int proc_id;               /* Process rank */
   int n_proc;                /* Number of processes */
   int check_circuit (int, int);

   MPI_Init (&argc, &argv);
   MPI_Comm_rank (MPI_COMM_WORLD, &proc_id);
   MPI_Comm_size (MPI_COMM_WORLD, &n_proc);

   count = 0;
   for (k = proc_id; k < 65536; k += n_proc)
      count += check_circuit (proc_id, k);

   MPI_Reduce (&count, &global_count, 1, MPI_INT, MPI_SUM, 0,
      MPI_COMM_WORLD); 
   printf ("Process %d of %d is done\n", proc_id, n_proc);
   fflush (stdout);
   MPI_Finalize();
   if (!proc_id) printf ("There are %d different solutions\n",
      global_count);
   return 0;
}

/* Return 1 if 'k'th bit of 'n' is 1; 0 otherwise */
#define EXTRACT_BIT(n,k) ((n&(1<<k))?1:0)

int check_circuit (int proc_id, int z) {
   int v[16];        /* Each element is a bit of z */
   int k;

   for (k = 0; k < 16; k++) v[k] = EXTRACT_BIT(z,k);
   if ((v[0] || v[1]) && (!v[1] || !v[3]) && (v[2] || v[3])
      && (!v[3] || !v[4]) && (v[4] || !v[5])
      && (v[5] || !v[6]) && (v[5] || v[6])
      && (v[6] || !v[15]) && (v[7] || !v[8])
      && (!v[7] || !v[13]) && (v[8] || v[9])
      && (v[8] || !v[9]) && (!v[9] || !v[10])
      && (v[9] || v[11]) && (v[10] || v[11])
      && (v[12] || v[13]) && (v[13] || !v[14])
      && (v[14] || v[15])) {
      printf ("%d) %d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d\n", proc_id,
         v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7],v[8],v[9],
         v[10],v[11],v[12],v[13],v[14],v[15]);
      fflush (stdout);
      return 1;
   } else return 0;
}
