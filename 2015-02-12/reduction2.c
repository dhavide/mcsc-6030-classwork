#include <stdio.h>
#include <math.h>
#include <omp.h>

/* Program to illustrate use of reduction for computing a maximum */
int
main (int argc, char *argv[])
{
  int k, n = 10;
  double x[10] = { -10.0, 11.0, 100.4, 50.0, 32.0,
		   43.1, 55.0, -1001.0, 56.3, 0.0 };
  double y = -INFINITY; /* from math.h */

  /* Print header for intermediate results */
  printf ("%2s\t%10s\t%5s\n", "k", "x[k]", "thread");
  #pragma omp parallel for reduction(max: y)
  for (k = 0; k < n; k++)
    {
      if (y<x[k])
          y = x[k];
      printf ("%2d\t%10.2f\t%2d\n", k, x[k], omp_get_thread_num ());
    }
  printf ("\nmaximum of x = %g\n", y);
  return 0;
}
