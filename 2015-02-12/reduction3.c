#include <stdio.h>
#include <stdbool.h>
#include <omp.h>

/* Program to illustrate use of reduction with logical or */
int
main (int argc, char *argv[])
{
  int k, n = 10;
  double x[10] = { -10.0, 11.0, 100.4, 50.0, 32.0,
		   43.1, 55.0, -1001.0, 56.3, 0.0 };
  bool anyzero = false; /* from stdbool.h */

  /* Print header for intermediate results */
  printf ("%2s\t%10s\t%5s\n", "k", "x[k]", "thread");
  #pragma omp parallel for reduction(||: anyzero)
  for (k = 0; k < n; k++)
    {
      anyzero = anyzero || (x[k]==0.0);
      printf ("%2d\t%10.2f\t%2d\n", k, x[k], omp_get_thread_num ());
    }
  printf("anyzero = %s\n", anyzero ? "true" : "false");
  return 0;
}
