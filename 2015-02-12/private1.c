#include <stdio.h>
#include <omp.h>
int main (int argc, char *argv[])
{
  int n, k, nthreads;
  double x[100], y;

  /* Specify number of threads to use: */
  nthreads = 2;
  #ifdef _OPENMP
  omp_set_num_threads (nthreads);
  printf ("Using OpenMP with %3d threads.\n", nthreads);
  #endif

  n = 7;
  y = 2.0;
  #pragma omp parallel for firstprivate(y) lastprivate(y)
  for (k = 0; k < n; k++)
    {
      y += 10.0;
      x[k] = y;
      #pragma omp critical
      {
	printf ("k = %3d, x[%3d] = %f\n", k, k, x[k]);
      }
    }
  printf ("At end, y = %f\n", y);
  return 0;
}
