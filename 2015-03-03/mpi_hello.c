#include <stdio.h>
#include <mpi.h>

int main (int argc, char* argv[]) {
   int proc_id;            /* Process rank */
   int n_proc;             /* Number of processes */

   /* Initializes communicator MPI_COMM_WORLD by default */
   MPI_Init (&argc, &argv);
   /* Stores *rank* of process in proc_id */
   MPI_Comm_rank (MPI_COMM_WORLD, &proc_id);
   /* Stores *number of processes* in n_proc */
   MPI_Comm_size (MPI_COMM_WORLD, &n_proc);

   printf ("Hello, World! from Process number %d of %d!\n", proc_id, n_proc);
   if (proc_id==1)
      printf ("I'm very special because I'm number %d!\n", proc_id);
 
   /* Shut down MPI processes */
   MPI_Finalize ();

   return 0;
}
