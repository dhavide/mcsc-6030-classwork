/*
 *   Circuit Satisfiability, OMP Version
 *
 *   This OMP program determines whether a circuit is
 *   satisfiable, that is, whether there is a combination of
 *   inputs that causes the output of the circuit to be 1.
 *   The particular circuit being tested is "wired" into the
 *   logic of function 'check_circuit'. All combinations of
 *   inputs that satisfy the circuit are printed.
 *
 *   Modified from MPI program by Michael J. Quinn
 *
 *   Last modification: 3 March 2015
 */

#ifndef _OPENMP
#include <omp.h>
#endif
#include <stdio.h>

int main (int argc, char *argv[]) {
  int k;
  int thread_id;               /* Thread identifier */
  int n_threads;               /* Number of threads */
  void check_circuit (int, int);

  #pragma omp parallel private(k, thread_id)
  {
    /* Defaults for serial case */
    n_threads = 1;
    thread_id = 0;
    #ifdef _OPENMP
      thread_id = omp_get_thread_num();
      n_threads = omp_get_num_threads();
    #endif
    #pragma omp for schedule(static, 1)
    for (k = 0; k < 65536; k++)
      check_circuit (thread_id, k);

    printf ("Process %d of %d is done\n", thread_id, n_threads);
    fflush (stdout);
  }
  return 0;
}

/* Return 1 if 'k'th bit of 'n' is 1; 0 otherwise */
#define EXTRACT_BIT(n,k) ((n&(1<<k))?1:0)

void check_circuit (int thread_id, int z) {
  int v[16];        /* Each element is a bit of z */
  int k;

  for (k = 0; k < 16; k++) v[k] = EXTRACT_BIT(z,k);
  if ((v[0] || v[1]) && (!v[1] || !v[3]) && (v[2] || v[3])
      && (!v[3] || !v[4]) && (v[4] || !v[5])
      && (v[5] || !v[6]) && (v[5] || v[6])
      && (v[6] || !v[15]) && (v[7] || !v[8])
      && (!v[7] || !v[13]) && (v[8] || v[9])
      && (v[8] || !v[9]) && (!v[9] || !v[10])
      && (v[9] || v[11]) && (v[10] || v[11])
      && (v[12] || v[13]) && (v[13] || !v[14])
      && (v[14] || v[15])) {
    printf ("%d) %d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d\n", thread_id,
            v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7],v[8],v[9],
            v[10],v[11],v[12],v[13],v[14],v[15]);
    fflush (stdout);
  }
}
