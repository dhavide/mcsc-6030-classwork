from mpi4py import MPI
# Note: in mpi4py, by default, not required to call mpi_init
comm = MPI.COMM_WORLD     # Assign alias to global communicator
n_proc = comm.Get_size()  # Query total number of processes
proc_id = comm.Get_rank() # Query identifier for this process
print "Hello, World! from Process number %d of %d!"  \
       % (proc_id,n_proc)
# Note: in mpi4py, by default, not required to call mpi_finialize either
