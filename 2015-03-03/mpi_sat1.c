/*
 *   Circuit Satisfiability, Version 1
 *
 *   This MPI program determines whether a circuit is
 *   satisfiable, that is, whether there is a combination of
 *   inputs that causes the output of the circuit to be 1.
 *   The particular circuit being tested is "wired" into the
 *   logic of function 'check_circuit'. All combinations of
 *   inputs that satisfy the circuit are printed.
 *
 *   Programmed by Michael J. Quinn
 *
 *   Last modification: 3 September 2002
 */

#include "mpi.h"
#include <stdio.h>

int main (int argc, char *argv[]) {
   int k;
   int proc_id;               /* Process rank */
   int n_proc;                /* Number of processes */
   void check_circuit (int, int);

   MPI_Init (&argc, &argv);
   MPI_Comm_rank (MPI_COMM_WORLD, &proc_id);
   MPI_Comm_size (MPI_COMM_WORLD, &n_proc);

   /* Use round robin distribution of integers... */
   for (k = proc_id; k < 65536; k += n_proc)
      check_circuit (proc_id, k);

   printf ("Process %d of %d is done\n", proc_id, n_proc);
   fflush (stdout);
   MPI_Finalize();
   return 0;
}

/* Return 1 if 'k'th bit of 'n' is 1; 0 otherwise */
#define EXTRACT_BIT(n,k) ((n&(1<<k))?1:0)

void check_circuit (int proc_id, int z) {
   int v[16];        /* Each element is a bit of z */
   int k;

   for (k = 0; k < 16; k++) v[k] = EXTRACT_BIT(z,k);
   if ((v[0] || v[1]) && (!v[1] || !v[3]) && (v[2] || v[3])
      && (!v[3] || !v[4]) && (v[4] || !v[5])
      && (v[5] || !v[6]) && (v[5] || v[6])
      && (v[6] || !v[15]) && (v[7] || !v[8])
      && (!v[7] || !v[13]) && (v[8] || v[9])
      && (v[8] || !v[9]) && (!v[9] || !v[10])
      && (v[9] || v[11]) && (v[10] || v[11])
      && (v[12] || v[13]) && (v[13] || !v[14])
      && (v[14] || v[15])) {
      printf ("%d) %d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d\n", proc_id,
         v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7],v[8],v[9],
         v[10],v[11],v[12],v[13],v[14],v[15]);
      fflush (stdout);
   }
}
