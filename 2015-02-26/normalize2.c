#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#ifdef _OPENMP
#include <omp.h>
#endif
#include "block.h"
/* Example of normalizing a vector using coarse-grain parallelism. */

int main (int argc, char *argv[]) {
    int n;
    double *x, *y;
    double norm, norm_thread, y_norm, y_norm_thread;
    int n_threads, thread_num;
    int k, k_start, k_end;

     /* Extract number of threads to use: */
    n_threads = 1;		/* default value for serial mode */
    #ifdef _OPENMP
      n_threads = strtol (argv[1], NULL, 10);
      omp_set_num_threads (n_threads);
      printf ("Using OpenMP with %2d threads\n", n_threads);
    #endif

    /* Extract size of arrays from command-line & allocate
       (note no error-checking) */
    n = (int) strtol (argv[2], NULL, 10);
    x = malloc (n*sizeof(*x));
    y = malloc (n*sizeof(*y));

    norm = 0.0;
    y_norm = 0.0;

    #pragma omp parallel private(k, norm_thread, k_start, k_end, \
            thread_num, y_norm_thread)
    {
        thread_num = 0;		/* required default in serial mode */
        #ifdef _OPENMP
          thread_num = omp_get_thread_num ();	/* unique ID for each thread */
        #endif
        /*
           Determine how many points to handle with each thread. The macros
           BLOCK_START and BLOCK_END identify the indices of the first and
           last elements of the array (respectively) handled by each thread.
           All elements are allocated to some thread explicitly.
        */
        k_start = BLOCK_START (thread_num, n_threads, n);
        k_end = BLOCK_END (thread_num, n_threads, n);

        #pragma omp critical
        printf ("Thread %d will take k = %u through k = %u\t(%d elements)\n",
                thread_num, k_start, k_end, 
                BLOCK_SIZE (thread_num, n_threads, n));

        /* Initialize local elements of vector x */
        for (k = k_start; k <= k_end; k++)
            x[k] = (double) (k + 1.0);	/* Offset to get values correct */

        norm_thread = 0.0;
        for (k = k_start; k <= k_end; k++)
            norm_thread += fabs (x[k]);

        /* Update global norm with value from each thread: */
        #pragma omp critical
        {
            norm += norm_thread;
            printf ("norm updated to: %g\n", norm);
        }

        /* Synchronise: ensure all threads update norm before proceeding: */
        #pragma omp barrier

        y_norm_thread = 0.0;

        for (k = k_start; k <= k_end; k++) {
            y[k] = x[k] / norm;
            y_norm_thread += fabs (y[k]);
        }

        /* Update global y_norm with value from each thread: */
        #pragma omp critical
        {
            y_norm += y_norm_thread;
            printf ("y_norm updated to: %g\n", y_norm);
        }
    } /* End of omp parallel block: threads merge/join into master thread */

    free (x);
    free (y);
    /* Output of final results */
    printf ("n = %d\n", n);
    printf ("norm of x = %10g\tn(n+1)/2 = %10g\n", norm,
            (double) n * (n + 1) * 0.5);
    printf ("y_norm should be 1.0:   y_norm = %10g\n", y_norm);

    return 0;
}
