/*
  Solve n ODEs and stop when one reaches value of 100.
  With debug statements including +++ left in for illustration.

  Try removing the first omp barrier and code may hang.
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#ifdef _OPENMP
  #include <omp.h>
#endif
#include "block.h"

int main (int argc, char *argv[]) {
    double *u, *c;
    double dt, u_max, u_max_thread;
    int n, n_threads, thread_num;
    int m, k, k_start, k_end, n_steps;

    /* Specify number of threads to use: */
    n_threads = 1;		/* default value for serial mode */
    #ifdef _OPENMP
      n_threads = strtol (argv[1], NULL, 10);
      omp_set_num_threads (n_threads);
      printf ("Using OpenMP with %2d threads\n", n_threads);
    #endif

    /* Extract size of arrays from command-line & allocate
       (note no error-checking) */
    n = (int) strtol (argv[2], NULL, 10); /* number of ODEs */
    u = malloc (n*sizeof(*u));
    c = malloc (n*sizeof(*c));

    /* initialise values in arrays */
    for (k = 0; k < n; k++) {
        u[k] = 1.0;
        c[k] = (double) (k - 0.5 * n);	/* some positive, some negative */
    }

    dt = 0.01;
    n_steps = 1000;
    u_max = -INFINITY;

    #pragma omp parallel private(k, m, u_max_thread, \
                                 k_start, k_end, thread_num)
    {
        thread_num = 0;		/* required default in serial mode */
        #ifdef _OPENMP
          thread_num = omp_get_thread_num ();	/* unique ID for each thread */
        #endif
        /*
           Determine how many points to handle with each thread. The macros
           BLOCK_START and BLOCK_END identify the indices of the first and
           last elements of the array (respectively) handled by each thread.
           All elements are allocated to some thread explicitly.
        */
        k_start = BLOCK_START (thread_num, n_threads, n);
        k_end = BLOCK_END (thread_num, n_threads, n);

        #pragma omp critical
        printf ("Thread %d will take k = %u through k = %u\t(%d elements)\n",
                thread_num, k_start, k_end, 
                BLOCK_SIZE (thread_num, n_threads, n));

        for (m = 0; m < n_steps; m++) {
            #ifdef DEBUG
              printf ("+++ begin loop      thread %3i m = %4i  u_max = %8.3f\n",
                    thread_num, m, u_max);
            #endif

            u_max_thread = -INFINITY; /* Every thread does this */
            #pragma omp single
            u_max = -INFINITY;  /* Only ONE thread does this */

            for (k = k_start; k <= k_end; k++) {
                u[k] = (1.0 + c[k] * dt) * u[k];
                u_max_thread = fmax (u_max_thread, u[k]);
            }

            /* update global umax with value from each thread: */
            #pragma omp critical
            u_max = fmax (u_max, u_max_thread);

            /* make sure all have updated umax before proceeding: */
            #pragma omp barrier
            /*
               Try removing the barrier above, in which case the code
               will sometimes hang --- quit out with ctrl-c
             */

            if (u_max > 100) {
                #ifdef DEBUG
                  printf ("+++ u_max>100: exit  thread %3i ", thread_num);
                  printf ("m = %4i  umax = %8.3f  u_max_thread = %8.3f\n",
                           m, u_max, u_max_thread);
                #endif
                break; /* Breaks out of for loop */
            }

            #ifdef DEBUG
               printf ("+++ u_max<100:       thread %3i ", thread_num);
               printf ("m = %4i  umax = %8.3f  u_max_thread = %8.3f\n",
                        m, u_max, u_max_thread);
            #endif
            /*
              Barrier here to ensure all threads are done testing u_max before
              going on to next iteration (where u_max is re-initialized to 0).
            */
            #pragma omp barrier
        } /* End of for loop */
    } /* End of omp parallel block: threads merge/join into master thread */

    free (u);
    free (c);
    printf ("Done!  u_max = %8.3f\n", u_max);

    return 0;
}
