#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#ifdef _OPENMP
#include <omp.h>
#endif
/* Example of normalizing a vector using fine-grain parallelism. */
int
main (int argc, char *argv[]) {
    int k, thread_num, n_threads, n;
    double norm, y_norm, *x, *y;

     /* Extract number of threads to use: */
    n_threads = 1;		/* default value for serial mode */
    #ifdef _OPENMP
      n_threads = strtol (argv[1], NULL, 10);
      omp_set_num_threads (n_threads);
      printf ("Using OpenMP with %2d threads\n", n_threads);
    #endif

    /* Extract size of arrays from command-line & allocate
       (note no error-checking) */
    n = (int) strtol (argv[2], NULL, 10);
    x = malloc (n*sizeof(*x));
    y = malloc (n*sizeof(*y));

    /* initialize  vector x */
    #pragma omp parallel for
    for (k = 0; k < n; k++)
        x[k] = (double) (k + 1);	/* Offset to get indexing correct */

    norm = 0.0;
    y_norm = 0.0;

    #pragma omp parallel private(k)
    {
        #pragma omp for reduction(+ : norm)
        for (k = 0; k < n; k++)
            norm += fabs (x[k]);

        /* omp barrier here not needed (implicit in for) */
        #pragma omp barrier

        #pragma omp for reduction(+ : y_norm)
        for (k = 0; k < n; k++) {
            y[k] = x[k] / norm;
            y_norm += fabs (y[k]);
        }
    } /* End of omp parallel block: threads merge/join into master thread */

    free (x);
    free (y);
    printf ("n = %d\n", n);
    printf ("norm of x = %10g\tn(n+1)/2 = %10g\n", norm,
            (double) n * (n + 1) * 0.5);
    printf ("y_norm should be 1.0:   y_norm = %10g\n", y_norm);

    return 0;
}
