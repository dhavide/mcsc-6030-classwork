/*   block.h
 *
 *   Header file with simple macros for block allocation schemes
 *
 *   Based on codes by Michael J. Quinn
 *
 */

/************************* MACROS **************************/

#define MIN(a,b)           ((a)<(b)?(a):(b))
/* N.B.: CEILING here assumes integer inputs and integer arithmetic */
#define CEILING(j,k)       (((j)+(k)-1)/(k))
/*
  Given n_proc processes/threads and n pieces of data indexed from 0 to
  n-1, BLOCK_START (resp., BLOCK_END) returns the first/lowest (resp.
  last/highest) index of the chunk of indices associated with proc_id.
  Note indexing here is assumed to be standard C 0-based indexing and that
  BLOCK_END returns the highest index of the chunk inclusively.
*/
#define BLOCK_START(proc_id, n_proc, n)  ((proc_id)*(n)/(n_proc))
#define BLOCK_END(proc_id, n_proc, n) (-1+BLOCK_START((proc_id)+1, n_proc, n))
#define BLOCK_SIZE(proc_id, n_proc, n) \
                     1 + BLOCK_END (proc_id, n_proc, n) - \
                         BLOCK_START (proc_id, n_proc, n)                       
/*
  Given n_proc processes/threads and n pieces of data indexed from 0 to
  n-1 and a (valid) index idx, BLOCK_OWNER returns the proc_id of the
  process associated with the chunk of data that contains idx.
  Note indexing here is assumed to be standard C 0-based indexing.
*/
#define BLOCK_OWNER(idx,n_proc,n) (((n_proc)*((idx)+1)-1)/(n))
